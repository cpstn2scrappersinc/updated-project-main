﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GorillaCsPicsMgr : MonoBehaviour {

	public List<GameObject> gorillaPics;

	public int picIndex;

	void Start () 
	{
		gorillaPics[picIndex].SetActive(true);
	}

	public void NextPic ()
	{
		if (picIndex < gorillaPics.Count) 
		{
			gorillaPics [picIndex].SetActive (true);
			//subtitle.text = introSubs[picIndex];
			picIndex += 1;
		} 
		else 
		{
			SceneManager.LoadScene("Puzzle 2");
		}
	}
}
