﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartIntro : MonoBehaviour {

	public void LoadIntro()
	{
		SceneManager.LoadScene("intro");
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
