﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleswapperCsPicMgr : MonoBehaviour {

	public List<GameObject> tsPics;
	//public List<string> introSubs;
	public int picIndex;
	//public Text subtitle;

	// Use this for initialization
	void Start () 
	{
		tsPics[picIndex].SetActive(true);
	}


	public void NextPic ()
	{
		if (picIndex < tsPics.Count) 
		{
			tsPics [picIndex].SetActive (true);
			//subtitle.text = introSubs[picIndex];
			picIndex += 1;
		} 
		else 
		{
			SceneManager.LoadScene("Puzzle 4");
		}
	}
}
