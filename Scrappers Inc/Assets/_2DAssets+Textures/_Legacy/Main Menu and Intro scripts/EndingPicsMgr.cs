﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingPicsMgr : MonoBehaviour {

	public List<GameObject> endingPics;

	public int picIndex;


	// Use this for initialization
	void Start () 
	{
		endingPics[picIndex].SetActive(true);
	}


	public void NextPic ()
	{
		if (picIndex < endingPics.Count) 
		{
			endingPics [picIndex].SetActive (true);
			//subtitle.text = introSubs[picIndex];
			picIndex += 1;
		} 
		else 
		{
			SceneManager.LoadScene("mainmenu");
		}
	}
}
