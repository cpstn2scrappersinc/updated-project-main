﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroPicsMgr : MonoBehaviour {

	public List<GameObject> introPics;
	//public List<string> introSubs;
	public int picIndex;
	//public Text subtitle;

	public AudioSource musicA;
	public AudioSource musicB;

	// Use this for initialization
	void Start () 
	{
		introPics[picIndex].SetActive(true);
		musicA.Play();
	}


	public void NextPic ()
	{
		if (picIndex < introPics.Count) {
			introPics [picIndex].SetActive (true);
			//subtitle.text = introSubs[picIndex];
			picIndex += 1;

			if (picIndex == 11) 
			{
				musicA.Stop();
			}
			if (picIndex == 12) 
			{
				musicB.Play();
			}
		} 
		else 
		{
			SceneManager.LoadScene("Puzzle 1");
		}
	}
}
