﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackGoatScript : MonoBehaviour
{
    // Has an order been fulfilled or not
    public bool isFulfilled = false;
    public bool isEmptyHanded = true;
    public bool nearGoat = false;

    // List of waypoints the goat must follow
    GameObject firstLoc, secondLoc;

    // Goat's move speed
    public float goatSpeed;

    // Variables and elements for rice goat
    public GameObject goatUI;
    Camera cam;

    // Misc values
    public GameObject pack;
    GameControllerScript gamCon;
    InventorySystem invSystem;
    //Animator unitAnim;

    // Use this for initialization
    void Start ()
    {
        // Set references
        gamCon = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();
        invSystem = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
        //unitAnim = GetComponentInChildren<Animator>();
        Locations(goatUI);

        cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update ()
    {
        float speed = (isFulfilled) ? goatSpeed * 1.5f : goatSpeed;

        // Moves the goat towards the target location (don't put anything in its way now)
        if (isHere(locationUpdate()) && !StopGoat())
        {
            //unitAnim.SetBool("isMoving", true);
            transform.position = Vector3.MoveTowards(this.transform.position, locationUpdate(), speed * Time.deltaTime);
            transform.LookAt(locationUpdate());
        }
        //unitAnim.SetBool("isMoving", false);

        // Turns the pack on if an order is fulfilled
        if (!goatUI)
            pack.SetActive(!isEmptyHanded);

        // Manage UI variables if it is a rice goat
        if (goatUI)
            GoatUIManager();
    }

    // Sets the location depending on the bool
    void Locations(bool x)
    {
        if(x)
        {
            firstLoc = gamCon.riceGoatFirstLoc;
            secondLoc = gamCon.riceGoatSecondLoc;
        }
        else
        {
            //firstLoc = gamCon.goatFirstLoc;
            //secondLoc = gamCon.goatSecondLoc;
        }
    }

    // Checks whether or not the object is close to the target location
    bool isHere(Vector3 location)
    {
        if (Vector3.Distance(this.transform.position, location) > 0.5f)
            return true;
        else return false;
    }

    // Stops the goat if it's near another goat
    bool StopGoat()
    {
        if (nearGoat && !isFulfilled)
            return true;
        else return false;
    }

    // Return's either the first or second location depending on the boolean value
    Vector3 locationUpdate()
    {
        return (isFulfilled) ? secondLoc.transform.position : firstLoc.transform.position;
    }

    // Manages UI for goat
    void GoatUIManager()
    {
        // Adds/Removes button and pack depending on fulfillment
        goatUI.SetActive(!isFulfilled);
        pack.SetActive(!isFulfilled);

        // Billboard effect
        goatUI.transform.LookAt(goatUI.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);
    }
    // ================================= Button functions =================================


    // Add rice to the player's inventory
    public void AddItemToInventory(GameObject itemToAdd)
    {
        // If the player's inventory is not full...
        if (!invSystem.InventoryFull())
        {
            Item itemScript = itemToAdd.GetComponent<Item>();

            // For each slot in the inventory...
            for (int i = 0; i < invSystem.slots.Length; i++)
            {
                // If this slot (slot[i]) is not filled yet...
                if (!invSystem.slots[i].isFull)
                {
                    // Set it to true / it is now filled
                    invSystem.slots[i].isFull = true;

                    // Adds a button corresponding to the item added in the inventory
                    Instantiate(itemScript.itemButton, invSystem.slots[i].transform, false);

                    // Add the gameobject to the inventory
                    invSystem.playerInventory[i] = itemToAdd;

                    // Stop the loop
                    isFulfilled = true;
                    break;
                }
            }
        }
        else Debug.Log("No space!");
    }

    // ================================= Trigger functions =================================
    private void OnTriggerEnter(Collider other)
    {
        // When it reaches the second location, remove goat
        if (other.gameObject.name == "GoatSecondLocation")
            Destroy(this.gameObject);

        // When it's near another goat
        if (other.CompareTag("Goat"))
            nearGoat = true;
    }

    private void OnTriggerStay(Collider other)
    {
        // When it reaches the second location, remove goat
        if (other.gameObject.name == "GoatSecondLocation")
            Destroy(this.gameObject);

        // When it's near another goat
        if (other.CompareTag("Goat"))
            nearGoat = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Goat"))
            nearGoat = false;
    }
}
