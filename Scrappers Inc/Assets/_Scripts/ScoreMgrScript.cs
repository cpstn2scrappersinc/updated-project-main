﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMgrScript : MonoBehaviour
{
	public Text uiText;
	public int curScore;
	public int addedScore;
	public GameObject gameCanvas;

	public GameObject popupPrefab;

	public GameObject perkNotifCanvas;

	PerksScript perksScript;
    
    void Start()
    {
    	perksScript = GameObject.Find("Player").GetComponent<PerksScript>();
		UpdateScoreUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IncreaseScore(int incAmt)
    {
    	addedScore = incAmt;
    	curScore += addedScore;

		GameObject scorePopup = Instantiate(popupPrefab,popupPrefab.transform.position, Quaternion.identity);
		scorePopup.GetComponent<ScorePopupScript>().addedScoreAmt = incAmt;
		scorePopup.transform.SetParent(gameCanvas.transform, false);

		if(curScore >= perksScript.requiredScore)
		{
    		perkNotifCanvas.SetActive(true);
			perksScript.PerkInfoTransparent(false);
    	}

		UpdateScoreUI();
    }

    public void UpdateScoreUI()
    {
    	uiText.text = "Score: " + curScore;
    }


}
