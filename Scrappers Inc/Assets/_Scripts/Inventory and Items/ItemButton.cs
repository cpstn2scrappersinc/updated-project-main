﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemButton : MonoBehaviour
{
    // The object this pertains to
    GameObject reference;

    // Place in the database the reference is on 
    public int id;

    // Cooldown timer, to be based on item cooldown
    float timer;

    public GameObject flashMgr;
    public GameObject tutMgr;

    // Script refs
    InventorySystem invSystem;
    SkillsScript skillsScript;
    NewPlayerScript player;
    PerksScript perksScript;
    ItemDatabase itemDB;
    Item itemInfo;
    Slot itemSlot;

    private void Start()
    {
		tutMgr = GameObject.Find("Tutorial Manager");
    	flashMgr = GameObject.Find("UIFlashMgr");
        player = GameObject.FindWithTag("Player").GetComponent<NewPlayerScript>();
		perksScript = GameObject.FindWithTag("Player").GetComponent<PerksScript>();
        skillsScript = GameObject.FindWithTag("Player").GetComponent<SkillsScript>();
        invSystem = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        itemSlot = GetComponentInParent<Slot>();
        reference = itemDB.itemDatabase[id];
        itemInfo = reference.GetComponent<Item>();
    }

    private void Update()
    {
    }

    public bool Useable()
    {
        switch (itemInfo.itemType)
        {
            // If the item is a fruit...
            case Item.ItemType.Fruit:
                return true;

            case Item.ItemType.Seed:
                if (!player.nearbySoil || player.nearbySoil.GetComponent<MainTreeScript>().isPlanted)
                    goto default;
                else return true;

            case Item.ItemType.PowerUp:
                if (!player.nearbySoil || player.nearbySoil.GetComponent<MainTreeScript>().stage > 3)
                    goto default;
                else return true;

            default:
                return false;
        }
    }

    public void UseItem()
    {

        switch (itemInfo.itemType)
        {
            // If the item is a fruit...
            case Item.ItemType.Fruit:
                // Consume the fruit for energy, depending on the fruit
                if(perksScript.fruitPerkUnlocked == true)
					skillsScript.curEnergy += (reference == itemDB.itemDatabase[0] || reference == itemDB.itemDatabase[7]) ? 25 : 40;
                else
                	skillsScript.curEnergy += (reference == itemDB.itemDatabase[0] || reference == itemDB.itemDatabase[7]) ? 15 : 30;

                //StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashGreen(0));

					if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 14)
					{
               			 tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(14);
            		}

				flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashGreen(0));
                break;

            case Item.ItemType.Seed:
                if (!player.nearbySoil || player.nearbySoil.GetComponent<MainTreeScript>().isPlanted)
                    goto default;
                else player.nearbySoil.GetComponent<MainTreeScript>().PlantTree(reference.GetComponent<Seed>());
                break;

            case Item.ItemType.PowerUp:
                reference.GetComponent<Powerup>().ActivatePowerUP(reference.GetComponent<Powerup>().powerUpType);
                break;

            default:
                Instantiate(reference, player.transform.position, Quaternion.identity);
                break;
        }

        // Removes the item from the inventory, then deletes the button
        invSystem.playerInventory[GetComponentInParent<Slot>().slotPosition] = null;
        Destroy(this.gameObject);

    }
}
