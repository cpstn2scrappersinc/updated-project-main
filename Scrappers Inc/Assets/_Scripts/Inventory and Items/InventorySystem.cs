﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySystem : MonoBehaviour
{
    public List<GameObject> playerInventory = new List<GameObject>();
    public Slot[] slots;

    // Player currency pouch and UI elements
    public int tokens = 0;
    public Text tokenText;

	

    // Use this for initialization
    void Start ()
    {
		
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdateTokens();
        InventoryKeys();
    }

    void InventoryKeys()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1) && slots[0].isFull)
            slots[0].GetComponentInChildren<ItemButton>().UseItem();
        if (Input.GetKeyDown(KeyCode.Alpha2) && slots[1].isFull)
            slots[1].GetComponentInChildren<ItemButton>().UseItem();
        if (Input.GetKeyDown(KeyCode.Alpha3) && slots[2].isFull)
            slots[2].GetComponentInChildren<ItemButton>().UseItem();
        if (Input.GetKeyDown(KeyCode.Alpha4) && slots[3].isFull)
            slots[3].GetComponentInChildren<ItemButton>().UseItem();
        if (Input.GetKeyDown(KeyCode.Alpha5) && slots[4].isFull)
            slots[4].GetComponentInChildren<ItemButton>().UseItem();
        if (Input.GetKeyDown(KeyCode.Alpha6) && slots[5].isFull)
            slots[5].GetComponentInChildren<ItemButton>().UseItem();
        if (Input.GetKeyDown(KeyCode.Alpha7) && slots[6].isFull)
            slots[6].GetComponentInChildren<ItemButton>().UseItem();
        if (Input.GetKeyDown(KeyCode.Alpha8) && slots[7].isFull)
            slots[7].GetComponentInChildren<ItemButton>().UseItem();
    }

    // Returns a boolean to determine if the inventory is full or not
    public bool InventoryFull()
    {
        foreach (GameObject item in playerInventory)
        {
            if (item == null)
                return false;
        }

        return true;
    }

    // Adds an item to the player's inventory
    public void AddItemToInventory(GameObject itemToAdd)
    {
        // If the player's inventory is not full...
        if (!InventoryFull())
        {
            Item itemScript = itemToAdd.GetComponent<Item>();
			
            // For each slot in the inventory...
            for (int i = 0; i < slots.Length; i++)
            {
                // If this slot (slot[i]) is not filled yet...
                if (slots[i].isFull)
                {
                    // Set it to true / it is now filled
                    slots[i].isFull = true;

                    // Adds a button corresponding to the item added in the inventory
                    Instantiate(itemScript.itemButton, slots[i].transform, false);
					

                    // Add the gameobject to the inventory
                    playerInventory[i] = itemToAdd;
					


                    // Stop the loop
                    break;
                }
            }
			

        }
        else Debug.Log("No space!");
    }

    // Returns the number of free spaces in the inventory
    public int FreeInventorySpaces()
    {
        int freeSpaces = 0;

        foreach (GameObject item in playerInventory)
        {
            if (item == null)
                freeSpaces++;
        }

        return freeSpaces;
    }

    void UpdateTokens()
    {
        // Clamps tokens to never be less than zero
        tokens = Mathf.Clamp(tokens, 0, tokens);

        // Updates token UI
        tokenText.text = "Pebbles: " + tokens;
    }
}
