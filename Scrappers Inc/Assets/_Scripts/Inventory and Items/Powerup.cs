﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : Item {

    // Defines what this powerup does
    public int powerUpType;
    public GameObject scoreMgr;

    // Script References
    NewPlayerScript playerScript;

	// Use this for initialization
	void Start ()
    {
        // Set this as default type
        this.itemType = ItemType.PowerUp;

		

        // Item Refs
        inventory = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        cam = Camera.main;

       

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // Adds the item to the player's inventory
    public override void AddItem()
    {
        base.AddItem();
    }

    // The powerup's abilities, depending on the int
    public void ActivatePowerUP(int powerType)
    {
        NewPlayerScript player = GameObject.FindWithTag("Player").GetComponent<NewPlayerScript>();

        switch (powerType)
        {
            // If the power up is a SHOVEL, add +4 to durability
            case 1:
                //GameObject.FindWithTag("Player").GetComponent<DiggingScript>().shovelDurab += 4;
                break;

            // If it's a manure powerup, and...
            case 2:
                // ... If the player is near a mound, that has been planted, and is fully grown...
				if (player.nearbySoil && player.nearbySoil.GetComponent<MainTreeScript>().stage >= 4 && player.nearbySoil.GetComponent<TreeFruitScript>().isFertilised == false)
                {
					scoreMgr = GameObject.Find("ScoreManager");
                    player.nearbySoil.GetComponent<TreeFruitScript>().isFertilised = true;   // Fertilise the tree;
					player.nearbySoil.GetComponent<TreeFruitScript>().StartCoroutine("RemoveFertilization");
                    scoreMgr.GetComponent<ScoreMgrScript>().IncreaseScore(100);
                }
                else goto default;
                break;

            default:
                //Instantiate(this.gameObject, player.transform.position, Quaternion.identity);
                break;
        }
    }
}
