﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : Item
{
    // Stages of the tree
    public GameObject[] treeStages;
    public GameObject fruit;
    public int[] growthTimes;
    public int[] scoreRewards;


    // Use this for initialization
    void Start ()
    {
        // Set this as default type
        this.itemType = ItemType.Seed;
        inventory = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        cam = Camera.main;
    }

    // Adds the item to the player's inventory
    public override void AddItem()
    {
        base.AddItem();
    }
}
