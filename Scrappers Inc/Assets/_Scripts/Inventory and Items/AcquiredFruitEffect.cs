﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AcquiredFruitEffect : MonoBehaviour
{
    // Start is called before the first frame update

    public Image fruitImg;
    public byte transparency;

    public float sizeInc;

    void Start()
    {
        StartCoroutine("Disappear");
    }

    // Update is called once per frame
    void Update()
    {
		transform.localScale += new Vector3(0.12f,0.12f,0);
		fruitImg.color = new Color32 (255,255,255, transparency);
		transparency -= 9;
    }

    public IEnumerator Disappear()
    {
    	yield return new WaitForSeconds(1);
    	Destroy(this.gameObject);
    }

}
