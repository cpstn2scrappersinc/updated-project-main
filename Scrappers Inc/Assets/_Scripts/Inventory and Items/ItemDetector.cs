﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDetector : MonoBehaviour
{
    // Item attatched in parent object
    Item item;

    private void Start()
    {
        item = GetComponentInParent<Item>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
            item.playerClose = true;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
            item.playerClose = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            item.playerClose = false;
    }
}
