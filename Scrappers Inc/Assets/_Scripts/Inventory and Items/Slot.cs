﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    // Position of the slot if it were on a list
    public int slotPosition;

    // What object the slot has + if it's full or not
    public bool isFull = false;
    public GameObject item;

    // Script reference
    InventorySystem inventory;

    public CanvasGroup blinker;

	// Use this for initialization
	void Start ()
    {
        inventory = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Makes the slot flash/blink if it has a useable item
        //ItemBlink();
        // If the slot has no button (a child), it is not full
        if (transform.childCount <= 0)
        {
            isFull = false;
            item = null;
        }
        else item = inventory.playerInventory[slotPosition];   //Elsewise, the item it is attributed to is the same as the item in the player's inventory
	}

    void ItemBlink()
    {
        if(GetComponentInChildren<ItemButton>() && GetComponentInChildren<ItemButton>().Useable())
            blinker.alpha = Mathf.Sin(6 * Time.time);
        else blinker.alpha = 0;
    }
}
