﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Item : MonoBehaviour
{
    // Inventory system reference. Found in player
    protected InventorySystem inventory;

    // Database reference
    protected ItemDatabase itemDB;

    // Values to be set in inspector
    // Item's button prefab
    public GameObject itemButton;
	public GameObject acquiredFx;
    
    //The name of the item
    public string itemName;

    //The item's description
    public string itemDesc;
    
    //Item's type
    public ItemType itemType;
    public enum ItemType
    {
        Seed,
        Fruit,
        PowerUp,
    }

    // Boolean if the player is closeby
    public bool playerClose = false;

    // Light indicator
    public Light itemLight;

    // Indicator if the player can pick the object up
    public GameObject itemPrompt;

    // Camera
    protected Camera cam;

    // Use this for initialization
    void Start ()
    {
        inventory = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        cam = Camera.main;
    }

    private void Update()
    {
        // Turn the prompt on if the player is close to the object
        // (Boolean for it is in ItemDetector script attatched to a collider in the gameObject's children)
        itemPrompt.gameObject.SetActive(playerClose);

        // Adds the item to the inventory if the F button is pressed
        if (Input.GetKeyDown(KeyCode.F) && playerClose)
            AddItem();

        // Billboard Effect
        itemPrompt.transform.LookAt(itemPrompt.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);

        // Rotates the object around
        //transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }

    // Adds the item to an empty space in the inventory
    public virtual void AddItem()
    {
        // For each slot in the inventory...
        for (int i = 0; i < inventory.slots.Length; i++)
        {
            // If this slot (slot[i]) is not filled yet...
            if (!inventory.slots[i].isFull)
            {
                // Set it to true / it is now filled
                inventory.slots[i].isFull = true;

                // Adds a button corresponding to the item added in the inventory
                Instantiate(itemButton, inventory.slots[i].transform, false);

                // Add the gameobject to the inventory
                inventory.playerInventory[i] = itemDB.itemDatabase[itemButton.GetComponent<ItemButton>().id];

                //Destroy the item and stop the loop
                Destroy(this.gameObject);
                break;
            }
        }
    }
}
