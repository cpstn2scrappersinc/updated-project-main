﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Orders: MonoBehaviour
{
    // List of items to be retrieved, use inspector to attatch prefabs to it
    public List<GameObject> order = new List<GameObject>();

    // Order Variables
    bool isInstantiated = false;
    string orderName;
    float maxTimer;
    float timer = 0;
    int tokenReward;
    int sustenanceIncrement;
    int scoreReward;

    // UI Elements
    public Image timerBar;
    public Text orderUIName;
    public List<Text> orderUIAmounts = new List<Text>();
    public List<int> orderAmounts = new List<int>();
    public List<GameObject> orderItemTypes = new List<GameObject>();

    // Script ref
    ItemDatabase itemDB;
    //GoatManager goatManager;
    GameControllerScript gamCon;
    InventorySystem invSystem;

	public GameObject sfxManager;
	public GameObject scoreMgr;
	public GameObject flashMgr;
	public GameObject tutMgr;

    private void Start()
    {
		tutMgr = GameObject.Find("Tutorial Manager");
		flashMgr = GameObject.Find("UIFlashMgr");
		scoreMgr = GameObject.Find("ScoreManager");
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        gamCon = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();
        invSystem = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
        //goatManager = GameObject.FindWithTag("Submissions").GetComponent<GoatManager>();

		sfxManager = GameObject.FindWithTag("SfxMgr");

        // Fill up the item types list with item database stuff
        orderItemTypes.Add(itemDB.itemDatabase[0]); // Coconut
        orderItemTypes.Add(itemDB.itemDatabase[1]); // Banana
        orderItemTypes.Add(itemDB.itemDatabase[3]); // Mango
        orderItemTypes.Add(itemDB.itemDatabase[7]); // Rice
    }

    private void Update()
    {
        UI();
    }

    // UI Elements updater
    void UI()
    {
        // Counts a timer down, destroying/cancelling the order when it reaches a certain limit
        timer += 1 * Time.deltaTime;
        if (timer > maxTimer)
        {
            //goatManager.SendGoatEmpty();
			sfxManager.GetComponent<SfxMgrScript>().PlaySfx(9);
            Destroy(this.gameObject);
        }

        // Update the order's timer
        timerBar.fillAmount = (maxTimer - timer) / maxTimer;

        // Count how many specific items are needed
        if (isInstantiated)
        {
            for (int i = 0; i < order.Count; i++)
            {
                for (int j = 0; j < orderItemTypes.Count; j++)
                {
                    if (order[i] == orderItemTypes[j])
                        orderAmounts[j]++;
                }
            }

            isInstantiated = false;
        }

        // Updates text values
        orderUIName.text = orderName;
        orderUIAmounts[0].text = ": " + orderAmounts[0];
        orderUIAmounts[1].text = ": " + orderAmounts[1];
        orderUIAmounts[2].text = ": " + orderAmounts[2];
        orderUIAmounts[3].text = ": " + orderAmounts[3];
    }

    // Adds the order to the list
    public void CreateNewOrder(List<GameObject> itemsToOrder, string nam, float timeLimit, int reward, int sustInc, int scoreRwrd)
    {
        // Sets the order variables from another function
        order = itemsToOrder;
        orderName = nam;
        maxTimer = timeLimit;
        isInstantiated = true;
        tokenReward = reward;
		sustenanceIncrement = sustInc;
		scoreReward = scoreRwrd;

        //var newList = list.FindAll(s => s.Equals("match"));
    }

    // Detects items from player's inventory
    bool DetectItem(List<GameObject> itemCost)
    {
        // Creates a duplicate of the inventory
        List<GameObject> playerInvCopy = new List<GameObject>(invSystem.playerInventory);

        // For every item in itemCost
        for (int i = 0; i < itemCost.Count; i++)
        {
            // If there's a copy of it in the player's inventory, remove it and continue the loop
            if (playerInvCopy.Contains(itemCost[i]))
                playerInvCopy.Remove(itemCost[i]);
            else return false;   // If there is no copy, return false and end the function prematurely
        }
        return true;   // Returns true if all items are there
    }

    // Complete an order in the list
    public void FinishOrder()
    {
        // If the player has the items...
        if(DetectItem(order))
        {
            //Remove the items from the player's inventory
            for (int j = 0; j < order.Count; j++)
            {
                for (int k = 0; k < invSystem.playerInventory.Count; k++)
                {
                    // Compare the COST LIST in orderList[i] with the player's inventory
                    // If the item in the inventory is the same as the one in the list...
                    if (order[j] == invSystem.playerInventory[k])
                    {
                        // Null the item, destroy the corresponding button and re-loop
                        invSystem.playerInventory[k] = null;
                        Destroy(invSystem.slots[k].GetComponentInChildren<ItemButton>().gameObject);
                        break;
                    }
                }
            }

            // Delete the order, reduce drought, award tokens, send pack goat on its way
            for (int i = 0; i < gamCon.orderList.Count; i++)
            {
                if(gamCon.orderList[i] == this)
                {
                    gamCon.orderList[i] = null;
                    break;
                }
            }
			//if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex >= 16)
			//	tutMgr.GetComponent<NewTutorialMgrScript>().DisableStuff();

			gamCon.ReduceDrought(sustenanceIncrement);

            invSystem.tokens += tokenReward;
            //goatManager.SendGoatOff();
			sfxManager.GetComponent<SfxMgrScript>().PlaySfx(8);


			if(timer < (maxTimer - maxTimer * 0.75f))
				scoreMgr.GetComponent<ScoreMgrScript>().IncreaseScore(scoreReward + 250);
			else
				scoreMgr.GetComponent<ScoreMgrScript>().IncreaseScore(scoreReward);

			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 17)
			{
					tutMgr.GetComponent<NewTutorialMgrScript>().FinishTutorial();
					tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex += 1;
			}

			flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashGreen(2));
            Destroy(this.gameObject);
        }
        else Debug.Log("You do not have the required items!");
    }

    /*/ Gets a text list of the orders
    public string GetOrder()
    {
        StringBuilder listMaker = new StringBuilder();
        foreach(GameObject items in order)
            listMaker.Append(items.GetComponent<Item>().itemName).Append("\n");
        
        return "Order: " + "\n" + listMaker.ToString();
    }*/
}
