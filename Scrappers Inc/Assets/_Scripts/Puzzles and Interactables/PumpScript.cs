﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PumpScript : MonoBehaviour
{
    // Static variables
    Vector3 firstLocation;
    public Vector3 secondLocation;   // Set in inspector
    float t = 0;

    // Time variable. Modify in inspector
    public float crossTime;

    // Use this for initialization
    void Start ()
    {
        // Set the initial and final location
        firstLocation = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Moves the pump back and forth
        t += Time.deltaTime;
        transform.localPosition = Vector3.Lerp(firstLocation, secondLocation, Mathf.PingPong(t / crossTime, 1));
    }
}
