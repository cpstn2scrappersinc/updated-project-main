﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorButton : MonoBehaviour
{

    // Initial transform of the switch/button
    Vector3 initialTransform;

    // Boolean if the button/plate is being pressed down
    public bool isPressed;

    // Use this for initialization
    void Start()
    {
        isPressed = false;
        initialTransform = transform.position;
    }

    // Collider trigger functions
    // If stood on by the player/object...
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PickUp>() || other.gameObject.GetComponent<NewPlayerScript>())
        {
            //interactableSfx.Play();

            // Button goes down
            transform.position = initialTransform - new Vector3(0, 0.05f, 0);

            // Bool becomes true
            isPressed = true;
        }
    }
    // If player/object remains on the button...
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<PickUp>() || other.gameObject.GetComponent<NewPlayerScript>())
        {
            // Stays down
            transform.position = initialTransform - new Vector3(0, 0.05f, 0);

            // Still true
            isPressed = true;
        }
    }
    // If player/object leaves...
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<PickUp>() || other.gameObject.GetComponent<NewPlayerScript>())
        {
            // Returns to "on" mode/goes back to original position
            transform.position = initialTransform;

            // Bool now false again
            isPressed = false;
        }
    }
}
