﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSwitch : Puzzle
{
    public override void Interact()
    {
        base.Interact();

        // Plays the audio file
        //interactableSfx.Play();

        // Toggles each object in the list
        foreach(GameObject gameObject in linkedObjects)
            gameObject.GetComponent<Puzzle>().Interact();
    }
}
