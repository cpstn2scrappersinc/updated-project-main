﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGrowingScript : MonoBehaviour
{/*
    // Water level variables
    public float curWaterLvl;
    public float maxWaterLvl;   // Set in inspector

    // Boolean values
    public bool isWatered = false;
    public bool hasSeed = false;
    public bool hasFruit = false;

    // Growth vectors. Set publics in inspector
    public Transform firstLocation;
    public Vector3 secondLocation;

    // Fruit spawning variables
    public GameObject fruit;
    public int fruitNum = 0;
    int fruitLimit = 0;

    // Misc
    public int points = 50;

    // Gameobject references
    GameObject tree;
    public GameObject attatchedTree;

    // Script references
    GameControllerScript gamCon;
    NewPlayerScript playerScript;

    private void Start()
    {
        // Initial values
        gamCon = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();
        playerScript = GameObject.FindWithTag("Player").GetComponent<NewPlayerScript>();
        curWaterLvl = 0;

        // Default amount if no max is set in inspector
        if (maxWaterLvl <= 0)
            maxWaterLvl = 100;
    }

    private void Update()
    {
        // Spawn tree function
        SpawnTree();
    }

    // Spawns the tree underground when the conditions are met
    void SpawnTree()
    {
        // If the curWaterLvl = maxWaterLvl and it hasn't been watered fully yet...
        if(CheckWater() && !isWatered)
        {
            tree = Instantiate(attatchedTree, firstLocation.position, Quaternion.identity, this.transform);

            // It is now fully watered
            isWatered = true;

            // Adds to the score
            gamCon.AddScore(points);
            gamCon.AddStats(4);
        }

        // Moves the tree up
        if(tree)
            tree.transform.localPosition = Vector3.MoveTowards(tree.transform.localPosition, secondLocation, 2.5f * Time.deltaTime);

        // Spawns fruits if toggled in inspector
        if (fruitLimit <= fruitNum && hasFruit && tree)
            SpawnFruits();
    }

    // Checks if the current water level of the mound is equal to the maxWaterLvl
    bool CheckWater()
    {
        if (curWaterLvl >= maxWaterLvl)
            return true;
        else return false;
    }

    // Spawns fruits around the area
    void SpawnFruits()
    {
        for (int x = 0; x < fruitNum; x++)
        {
            // Set random numbers for vector location
            float rand1 = Random.Range(-2, 2);
            float rand2 = Random.Range(5, 6);
            Vector3 loc = tree.transform.position;
            loc.x += rand1;
            loc.y += rand2;
            loc.z += x+2;

            // Instantiate
            Instantiate(fruit, loc, Quaternion.identity, this.transform.parent);

            // Add to the limit
            fruitLimit++;
        }
    }

    public void WaterPlant()
    {
        if(!isWatered && hasSeed)
            curWaterLvl += 150 * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        // If the object is water...
        if(other.gameObject.tag == "WaterSpray" && !isWatered && hasSeed)
            curWaterLvl += 10 * Time.deltaTime;

        // If the object is a coconut/seed...
        if (other.gameObject.tag == "Seed" && !hasSeed)
        {
            // Remove the seed from the player if it is being carried
            if(playerScript.pickedUpObject == other.gameObject)
            {
                // Remove it from the player's "hands"
                playerScript.pickedUpObject.transform.parent = null;

                // Set player values back to default
                playerScript.pickedUpObject = null;
                playerScript.isHandsFull = false;
            }

            // Plants it in the mound
            other.gameObject.transform.parent = this.transform;
            other.gameObject.transform.position = this.transform.position;
            Destroy(other.gameObject.GetComponent<PickUp>());

            // The mound is ready to be watered
            hasSeed = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        // If the object is water...
        if (other.gameObject.tag == "WaterSpray" && !isWatered && hasSeed)
            curWaterLvl += 10 * Time.deltaTime;
    }*/
}
