﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootBridge : Puzzle
{
    // Position and movement vectors
    private Vector3 currentPos;
    private Vector3 originalPos;
    public Vector3 targetPos;
    private float speed;

    private void Start()
    {
        // Set the original position to where it is, currently, on the scene
        originalPos = this.transform.localPosition;

        // If there is no set target, the set target is just below the original pos
        if (targetPos == Vector3.zero)
            targetPos = new Vector3(transform.localPosition.x, transform.localPosition.y - 2.0f, transform.localPosition.z);

        // Set the current position as the original (since it didn't move yet)
        currentPos = originalPos;

        // Base speed
        speed = 6.0f;
    }

    void Update()
    {
        // Moves the object
        ObjAnim(currentPos);
    }

    public override void Interact()
    {
        base.Interact();

        // Sets the position to a new target, moving it
        currentPos = targetPos;
    }

    // Moves the object towards the set target destination
    void ObjAnim(Vector3 direction)
    {
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, direction, speed * Time.deltaTime);
    }
}
