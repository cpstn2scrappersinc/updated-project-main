﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerScript : MonoBehaviour
{
    public float rotateSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Rotation
        transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
    }

    // Collision functions to ensure the player doesn't slip off the platform
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // Removes velocity on initial contact 
            collision.gameObject.GetComponent<Rigidbody>().velocity -= collision.gameObject.GetComponent<Rigidbody>().velocity;

            // Sets player to be child of platform
            collision.transform.parent = this.transform;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent = this.transform;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (!collision.gameObject.GetComponent<Pickup>())
                collision.transform.parent = null;
        }
    }
}
