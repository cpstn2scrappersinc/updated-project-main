﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTide : MonoBehaviour
{
    public TidePlatform tidePlatform;

        // Triggers when the object falls into the zone
        private void OnTriggerEnter(Collider other)
    {
        // If the object is a player, send him back to the last checkpoint
        if (other.GetComponent<NewPlayerScript>())
            // Reset the puzzle
            tidePlatform.isActive = false;
    }
}
