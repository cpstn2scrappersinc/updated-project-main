﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RisingTideScript : MonoBehaviour
{/*
    // Script references
    GameControllerScript gamCon;
    public TidePlatform tidePlatform;   // Set in inspector

    // Position variables
    Vector3 initialPos;
    public Vector3 endPos;   // Set in inspector

    private void Start()
    {
        // Set initial values
        gamCon = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();

        // Initial vector variables
        initialPos = this.transform.localPosition;
    }

    private void Update()
    {
        // Move the water up per initial-end positions, based on the boolean value in TidePlatform script
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, SetCurrentPos(tidePlatform.isActive), 0.5f * Time.deltaTime);
    }

    // Returns a vector value based on a boolean
    public Vector3 SetCurrentPos(bool active)
    {
        if (active) return endPos;
        else return initialPos;
    }

    // Triggers when the object falls into the zone
    private void OnTriggerEnter(Collider other)
    {
        // If the object is a player, send him back to the last checkpoint
        if (other.GetComponent<NewPlayerScript>())
        {
            other.transform.position = gamCon.currentCheckpoint.transform.position;
            other.transform.rotation = Quaternion.identity;
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;

            // Reset the puzzle
            tidePlatform.isActive = false;
        }
    }*/
}
