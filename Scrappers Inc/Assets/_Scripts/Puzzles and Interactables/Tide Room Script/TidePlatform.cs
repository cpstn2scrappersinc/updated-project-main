﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TidePlatform : MonoBehaviour
{
    // Script references. Set these in the inspector.
    public RisingTideScript tideScript;

    // Door references, to be set in the inspector
    public GameObject door;
    Vector3 initialPos;
    public Vector3 endPos;   // Set in inspector

    private void Start()
    {
        // Initial vector variables
        initialPos = door.transform.localPosition;
    }

    private void Update()
    {
        // Move the door up per initial-end positions, based on the boolean value in TidePlatform script
        door.transform.localPosition = Vector3.MoveTowards(door.transform.localPosition, SetCurrentPos(isActive), 5 * Time.deltaTime);
    }

    // Returns a vector value based on a boolean
    public Vector3 SetCurrentPos(bool active)
    {
        if (active) return endPos;
        else return initialPos;
    }

public bool isActive = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
            isActive = true;
    }
}
