﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    // List of objects linked to the object itself (if applicable)
    public List<GameObject> linkedObjects = new List<GameObject>();

    // What kind of interactable is it?
    public PuzzleType type;
    public enum PuzzleType
    {
        ToggleSwitch,
        Checkpoint,
        Pickup,
    }

    // Main interaction function
    public virtual void Interact()
    {
    }
}