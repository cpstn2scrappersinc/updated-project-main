﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{
    // Type of platform the object is
    public enum PlatformType
    {
        SwitchBased,
        Standalone,
        Crumbling,
    }
    public PlatformType platformType;

    // Misc values for modes
    public bool isToggled = false;
    public bool isStoodOn = false;
    public float crumbleTime = 2.0f;
    float t = 0;

    // For moving platforms
    // Starting and ending locations for platform movement
    Vector3 firstLocation;
    public Vector3 secondLocation;

    // Time it takes to move between locations
    public float crossTime;

    // Use this for initialization
    void Start ()
    {
        firstLocation = this.transform.position;
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Set the platform's type on the inspector
        switch(platformType)
        {
            case PlatformType.SwitchBased:
                SwitchBased();
                break;

            case PlatformType.Standalone:
                Standalone();
                break;

            case PlatformType.Crumbling:
                Crumbling();
                break;
        }
    }

    // Standalone version
    void Standalone()
    {
        // Moves the platform
        t += Time.deltaTime;
        transform.position = Vector3.Lerp(firstLocation, secondLocation, Mathf.PingPong(t / crossTime, 1));
    }

    // For use on switch-based platforms
    void SwitchBased()
    {
        // isToggles is activated via a switch
        if(isToggled)
        {
            // Moves the platform
            t += Time.deltaTime;
            transform.position = Vector3.Lerp(firstLocation, secondLocation, Mathf.PingPong(t / crossTime, 1));
        }
    }

    // Sets the platform tumbling down when the player stands on it
    void Crumbling()
    {
        // When the player stands on the platform, begin counting down
        if(isStoodOn)
            t += Time.deltaTime;

        // When the timer exceeds the set time...
        if(t >= crumbleTime)
        {
            // Remove rigidbody constraints and turn gravity on
            this.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            this.gameObject.GetComponent<Rigidbody>().useGravity = true;
            this.gameObject.GetComponent<Rigidbody>().isKinematic = false;

            // Does something after a set amount of time
            if (t >= crumbleTime + 2.0f)
            {
                // Reset defaults
                isStoodOn = false;
                this.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                this.gameObject.GetComponent<Rigidbody>().useGravity = false;
                this.gameObject.GetComponent<Rigidbody>().isKinematic = true;

                // Remove player from object parent
                if(this.gameObject.GetComponentInChildren<NewPlayerScript>())
                    this.gameObject.GetComponentInChildren<NewPlayerScript>().gameObject.transform.parent = null;

                // Clone the object, the destroy it
                Instantiate(this.gameObject, firstLocation, Quaternion.identity);
                Destroy(this.gameObject);
            }
        }
    }

    // Collision functions to ensure the player doesn't slip off the platform
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            // Removes velocity on initial contact 
            collision.gameObject.GetComponent<Rigidbody>().velocity -= collision.gameObject.GetComponent<Rigidbody>().velocity;

            // Sets player to be child of platform
            collision.transform.parent = this.transform;
            isStoodOn = true;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent = this.transform;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (!collision.gameObject.GetComponent<Pickup>())
                collision.transform.parent = null;
        }
    }
}
