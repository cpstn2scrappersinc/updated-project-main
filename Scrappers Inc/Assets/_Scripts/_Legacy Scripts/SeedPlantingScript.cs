﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedPlantingScript : MonoBehaviour
{/*
    // Script reference
    TreeGrowingScript treeScript;
    GameControllerScript gamCon;

    // UI Reference
    public GameObject choices;
    public GameObject waterPrompt;
    bool choiceMade = false;

    Camera cam;

    private void Start()
    {
        treeScript = GetComponentInParent<TreeGrowingScript>();
        gamCon = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();
        cam = Camera.main;
        choices.SetActive(false);
    }

    private void Update()
    {
        // Billboard effect
        choices.transform.LookAt(choices.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);
        waterPrompt.transform.LookAt(waterPrompt.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);
    }

    private void OnTriggerEnter(Collider other)
    {
        // If the player enters the collider...
        if (other.tag == "Player" && !choiceMade && !treeScript.isWatered)
            choices.SetActive(true);
        else if (other.tag == "Player" && choiceMade && !treeScript.isWatered)
            waterPrompt.SetActive(true);
    }

    private void OnTriggerStay(Collider other)
    {
        // If the player enters the collider...
        if (other.tag == "Player" && !choiceMade)
        {
            choices.SetActive(true);

            if (Input.GetKeyDown("1"))
            {
                gamCon.AddStats(1);
                SetTree(gamCon.treeList[0], gamCon.fruitList[0]);
            }
            if (Input.GetKeyDown("2"))
            {
                gamCon.AddStats(2);
                SetTree(gamCon.treeList[1], gamCon.fruitList[1]);
            }
            if (Input.GetKeyDown("3"))
            {
                gamCon.AddStats(3);
                SetTree(gamCon.treeList[2], gamCon.fruitList[2]);
            }
        }
        else if (other.tag == "Player" && choiceMade && !treeScript.isWatered)
            waterPrompt.SetActive(true);
    }
    private void OnTriggerExit(Collider other)
    {
        // If the player enters the collider...
        if (other.tag == "Player")
        {
            choices.SetActive(false);
            waterPrompt.SetActive(false);
        }
    }

    // Sets the type of tree to sprout, as well as the fruits spawned
    private void SetTree(GameObject tree, GameObject fruit)
    {
        treeScript.attatchedTree = tree;
        treeScript.fruit = fruit;
        treeScript.fruitNum = 1;
        treeScript.hasSeed = true;
        treeScript.hasFruit = true;
        choices.SetActive(false);
        choiceMade = true;
        return;
    }*/

}
