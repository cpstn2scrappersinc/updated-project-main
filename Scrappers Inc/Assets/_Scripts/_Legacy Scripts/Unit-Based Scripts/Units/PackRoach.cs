﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackRoach : Unit {

	// Use this for initialization
	void Start () 
	{
		SetStats (50, 3, 10);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (isDead())
			Death ();
	}

	public override void Death()
	{
		Destroy(this.gameObject);
		Instantiate (lootDrop, transform.position, Quaternion.identity);
		base.Death ();
	}
}
