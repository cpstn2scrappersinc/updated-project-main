﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : Unit {
		
	// Physics variables
	Vector3 movement;
	Rigidbody playerRigidbody;

	// Player stats. 
	public float curOxygen;
	float maxOxygen;

	// Misc Variables
	int floorMask;
	float camRayLength = 100f;

	// TEMP VALUES until player stats and other scripts/assets are out
	public float weaponCD;
	public GameObject bullet;

	// Use this for initialization
	void Start () 
	{
		playerRigidbody = this.GetComponent<Rigidbody> ();
		floorMask =  LayerMask.GetMask ("Floor");

		//Set player stats
		SetStats (100, 3, 10);
		maxOxygen = 100.0f;
		curOxygen = maxOxygen;

		// TEMP VALUES until player stats and other scripts are out
		weaponCD = 1.5f;
	}
	
	// Update is called once per frame
	void Update()
	{
		// Store the input axes.
		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");

		Move (h,v);
		Turning ();
		//FireWeapon ();
		//OxygenMechanics ();
	}

	// Moves the player via direction keys
	void Move (float h, float v)
	{
		// Set the movement vector based on the axis input.
		movement.Set (h, 0f, v);

		// Normalise the movement vector and make it proportional to the speed per second.
		movement = movement.normalized * this.unitSpeed.finalValue * Time.deltaTime;

		// Move the player to it's current position plus the movement.
		playerRigidbody.MovePosition (transform.position + movement);
	}


	// Turns the player towards the mouse.
	void Turning ()
	{
		// Create a ray from the mouse cursor on screen in the direction of the camera.
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);

		// Create a RaycastHit variable to store information about what was hit by the ray.
		RaycastHit floorHit;

		// Perform the raycast and if it hits something on the floor layer...
		if(Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
		{
			// Create a vector from the player to the point on the floor the raycast from the mouse hit.
			Vector3 playerToMouse = floorHit.point - transform.position;

			// Ensure the vector is entirely along the floor plane.
			playerToMouse.y = 0f;

			// Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
			Quaternion newRotation = Quaternion.LookRotation (playerToMouse);

			// Set the player's rotation to this new rotation.
			playerRigidbody.MoveRotation (newRotation);
		}
	}

	// Weapon firing function, temporary for testing
	void FireWeapon()
	{
		if (Input.GetButton ("Fire1") && Time.time > weaponCD) 
		{
			weaponCD = Time.time + 0.1f;
			Instantiate (bullet, this.transform.position, this.transform.rotation);
			curOxygen -= 1.0f;
		}
	}

	// Oxygen Mechanics
	void OxygenMechanics()
	{
		// Clamps oxygen value to be 100 at max
		if (curOxygen > maxOxygen)
			curOxygen = maxOxygen;

		// Drains oxygen over time
		curOxygen -= Time.deltaTime * 0.5f;
	}
}
