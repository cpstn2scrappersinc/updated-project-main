﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float movementSpeed = 5.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 playerPos = this.transform.position;
        if (Input.GetKey("w")) {
            transform.Translate(Vector3.forward * Time.deltaTime* movementSpeed);
        }
        if (Input.GetKey("s")) {
            transform.Translate(Vector3.back * Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey("a")) {
            transform.Translate(Vector3.left * Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey("d")) {
            transform.Translate(Vector3.right * Time.deltaTime * movementSpeed);
        }
	}
}
