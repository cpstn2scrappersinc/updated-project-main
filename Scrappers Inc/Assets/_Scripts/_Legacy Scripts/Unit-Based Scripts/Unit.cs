﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unit : MonoBehaviour {

	//HP Stat variables
	public float curHP;
	public float maxHP;

	//Regular Stat variables
	public Stat unitSpeed;
	public Stat unitDamage;

	// TEMP: Enemy Loot
	public GameObject lootDrop;

	public void SetStats(int hp, int speed, int damage)
	{
		this.maxHP = (float)hp;
		this.curHP = this.maxHP;

		this.unitSpeed.baseValue = (float)speed;
		this.unitDamage.baseValue = (float)damage;
	}
		
	public bool isDead()
	{
		return (curHP <= 0);
	}

	public virtual void Death()
	{
	}

	public void DamageHP(float damageAmount)
	{
		curHP -= damageAmount;
	}
}
