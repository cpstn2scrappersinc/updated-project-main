﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour {

	// Temp projectile script

	// Use this for initialization
	void Start () 
	{
		Destroy (this.gameObject, 2.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.transform.position += transform.forward * 15.0f * Time.deltaTime;
	}
}
