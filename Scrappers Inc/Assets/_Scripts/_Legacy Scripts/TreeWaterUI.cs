﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreeWaterUI : MonoBehaviour
{/*
    // References. Set in inspector
    TreeGrowingScript treeScript;
    public GameObject waterCanvas;
    public Image waterMeter;

    Camera cam;

	// Use this for initialization
	void Start ()
    {
        treeScript = GetComponent<TreeGrowingScript>();
        cam = Camera.main;
    }

    // Billboard effect
    private void LateUpdate()
    {
        waterCanvas.transform.LookAt(waterCanvas.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);
    }

    // Update is called once per frame
    void Update ()
    {
        // If the water level is neither at 0 nor at max
        if (treeScript.curWaterLvl > 0 && treeScript.curWaterLvl < treeScript.maxWaterLvl)
        {
            // Turn the slider on
            waterCanvas.SetActive(true);

            // Sets the water meter for the mound
            waterMeter.fillAmount = treeScript.curWaterLvl / treeScript.maxWaterLvl;
        }
        else waterCanvas.SetActive(false);   // Turn the slider off
    }*/
}
