﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject loadSceneBG;
    public Image loadBar;
    public float setLoadTime;
    public bool isEnding = false;
    public Text factText;
    public List<string> facts = new List<string>();
    public GameObject MenuUI;
    float timer = 0;
    //CharMgrScript charManager;

    private void Start()
    {
        // Sets a default time if none is added
        if (setLoadTime == 0)
            setLoadTime = 3.0f;
        Cursor.visible = false;
        //charManager = this.GetComponentInChildren<CharMgrScript>();
    }

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
            ToggleMenu();
            
        // If the game is supposed to end, start the artificial load time
        if (isEnding)
            ArtificialLoad(setLoadTime);
    }

    // Changes the scene
    public void ChangeScene()
    {
        // Sets the game to end
        isEnding = true;

        // Generates random text for facts
        int randText = Random.Range(0, facts.Count - 1);
        factText.text = facts[randText];
    }

    // Reloads the current scene
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Gets the int of the next scene after the current
    int nextSceneInt()
    {
        if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings)
            return SceneManager.GetActiveScene().buildIndex + 1;
        else return 0;
    }

    // Makes loading time longer by length provided
    void ArtificialLoad(float length)
    {
        Cursor.visible = true;

        // Turn the load screen on
        loadSceneBG.SetActive(true);

        // Run the timer then fill the load bar along with it
        timer += 1 * Time.deltaTime;
        loadBar.fillAmount = timer / length;

        // When the timer reaches a point, start the coroutine to load the next scene
        if (timer >= length)
            StartCoroutine(LoadNextScene());
    }
    
    // Changes the scene, moving it to the next level after loading it
    IEnumerator LoadNextScene()
    {
        yield return new WaitForSeconds(0.5f);
        // Load the next scene
        AsyncOperation operation = SceneManager.LoadSceneAsync(nextSceneInt());

        // While the scene isn't loaded yet
        while (!operation.isDone)
        {
            float loadProgress = Mathf.Clamp01(operation.progress / 0.9f);
            loadBar.fillAmount = loadProgress;

            yield return null;
        }
    }

// Toggles the active state of the Menu Window
    public void ToggleMenu()
    {
        MenuUI.SetActive(!MenuUI.activeSelf);
        Cursor.visible = !Cursor.visible;
    }

// Returns the game to the Main Menu scenes
    public void QuitToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
