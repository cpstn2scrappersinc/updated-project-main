﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Base class for pickups/powerups
public class PowerUp : MonoBehaviour 
{
	// Value of the object. Set in inspector
	public float pickupValue;

	// Override for the object. Adds value to the relevant stat
	public virtual void PickupEffect(Collider other)
	{
		Destroy (this.gameObject);
	}

	// Trigger Effect. Happens when objects touch
	protected void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
			PickupEffect(other);
	}
}
