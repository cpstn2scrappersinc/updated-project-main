﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lvl1TutorialManager : MonoBehaviour
{
    public List<CanvasGroup> tutorialImages = new List<CanvasGroup>();
    public int curImage;
    public Spider spider;
    public bool spiderJumped = false;

    IEnumerator moveControlCoroutine;
    IEnumerator jumpTip;

	// Use this for initialization
	void Start ()
    {
        //Set variables
        spider = GameObject.FindWithTag("Spider").GetComponent<Spider>();

        // Set coroutines
        moveControlCoroutine = FadeInOutImage(3f, tutorialImages[0]);
        StartCoroutine(moveControlCoroutine);

        jumpTip = FadeInOutImage(3f, tutorialImages[1]);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(spider.isMounted && !spider.isGrounded && !spiderJumped)
        {
            StopCoroutine(moveControlCoroutine);
            tutorialImages[0].gameObject.SetActive(false);
            StartCoroutine(jumpTip);
            spiderJumped = true;
        }
    }

    // Fades images in and out
    IEnumerator FadeInOutImage(float time, CanvasGroup toShow)
    {
        float i = 0;
        float rate = 1 / time;

        while(i < 1)
        {
            toShow.alpha = i;
            i += Time.deltaTime * rate;
            yield return 0;
        }
        yield return new WaitForSeconds(time);
        toShow.gameObject.SetActive(false);
        yield break;
    }
}
