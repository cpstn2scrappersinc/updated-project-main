﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserReceiverScript : LaserHitBase
{

    public List<GameObject> guideLights = new List<GameObject>();

    private void Update()
    {
        LightsOn();
    }

    public override void RayHit()
    {
        base.RayHit();
        isHit = true;
    }

    public void LightsOn()
    {
        foreach (GameObject light in guideLights)
        {
            light.SetActive(isHit);
        }
    }
}
