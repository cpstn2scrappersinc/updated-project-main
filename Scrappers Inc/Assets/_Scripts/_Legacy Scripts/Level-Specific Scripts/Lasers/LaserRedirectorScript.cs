﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRedirectorScript : LaserHitBase
{
    // Main raycast and raycast variables
    public Ray laserRay = new Ray();
    RaycastHit[] curLaserHits;
    RaycastHit[] lastLaserHits;
    public int laserMask;
    public int laserStop;

    // Secondary raycast and variables
    Ray secondRay = new Ray();
    RaycastHit maxDistPoint;
    public float maxDist;

    // Visible line in game
    public LineRenderer laserLine;

    public GameObject curRedirector;
    public GameObject lastRedirector;

    // Use this for initialization
    void Start()
    {
        // Set Variables
        laserLine = GetComponent<LineRenderer>();
        laserMask = LayerMask.GetMask("LaserMask");
        laserStop = LayerMask.GetMask("LaserStop");
    }

    // Update is called once per frame
    void Update()
    {
        if (curRedirector != null)
            RedirectorProject(curRedirector);
    }

    public override void RayHit()
    {
        // Initial Line position
        laserLine.enabled = true;
        laserLine.SetPosition(0, transform.position);

        // Ray origin and direction
        laserRay.origin = transform.position;
        laserRay.direction = transform.forward;
        secondRay.origin = transform.position;
        secondRay.direction = transform.forward;

        // Project a second ray out to determine the max distance
        // If it hits an object with the LaserStop mask
		
        if (Physics.Raycast(secondRay, out maxDistPoint, 1000f, laserStop))
        {
        	
             // Check for the redirector script
            if (maxDistPoint.collider.GetComponent<LaserRedirectorScript>())
            {
                // Set current redirector to it
                curRedirector = maxDistPoint.collider.gameObject;

                // If there was no redirector before, set it as the last as well
                if (lastRedirector == null)
                    lastRedirector = curRedirector;
                else if (lastRedirector != curRedirector) // If the last redirector hit isn't the current one, turn the last one off and replace its
                {
                    lastRedirector.GetComponent<LaserRedirectorScript>().TurnLaserOff();
                    lastRedirector = curRedirector;
                }
                // Set max distance and the line's end point
                //maxDist = maxDistPoint.point.magnitude;
                //laserLine.SetPosition(1, maxDistPoint.point);
            }
            else
            {
                // Set a base distance and a base end point. Turn off then remove the saved redirector
                //maxDist = 100f;
                if (curRedirector != null)
                {
                    curRedirector.GetComponent<LaserRedirectorScript>().TurnLaserOff();
                    lastRedirector.GetComponent<LaserRedirectorScript>().TurnLaserOff();
                }
                
                curRedirector = null;
                lastRedirector = null;
                //laserLine.SetPosition(1, secondRay.origin + secondRay.direction * maxDist);
            }
            
            maxDist = maxDistPoint.point.magnitude;
            laserLine.SetPosition(1, maxDistPoint.point);
        }
        

        // Send out a second raycast that takes all hits within the line. Ignores other colliders except those with LaserMask
        curLaserHits = Physics.RaycastAll(secondRay, maxDist, laserMask);

        // Transform it into a HashSet, then compare it to lastLaserHits to remove all uniques. Turn off all the uniques.
        HashSet<RaycastHit> curSet = new HashSet<RaycastHit>(curLaserHits);
        if (lastLaserHits != null)
        {
            // Compare with lastLaserHits to remove all non-unique elements in array
            curSet.SymmetricExceptWith(lastLaserHits);

            // Turn off the uniques
            foreach(RaycastHit hit in curSet)
                    hit.transform.GetComponent<LaserHitBase>().isHit = false;
        }

        // Set a new lastLaseHits array
        lastLaserHits = curLaserHits;

        // For every hit object in the laserHits array
        for (int x = 0; x < curLaserHits.Length; x++)
        {
            // Activate RayHit()
            curLaserHits[x].transform.GetComponent<LaserHitBase>().RayHit();
            Debug.Log("Working");
        }
    }

    // Causes the redirector to project its laser
    void RedirectorProject(GameObject redirector)
    {
		//Debug.Log("hello");
        redirector.GetComponent<LaserRedirectorScript>().RayHit();
    }

    // Resets the redirector's line (turns it off);
    public void TurnLaserOff()
    {
        laserLine.enabled = false;
    }
}
