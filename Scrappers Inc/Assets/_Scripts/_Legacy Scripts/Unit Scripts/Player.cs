﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : PlayableUnit
{

    // Place where a picked up object is put on
    public GameObject pickedUpObject;
    public GameObject mountObject;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        unitAnim = GetComponentInChildren<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
    }

    // Keyboard command for E button
    public override void ECommand()
    {
        base.ECommand();

        // F Command. Mounts a minion
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (currentObject == null)
                throw new UnityException("No mount in range");

            if (mountObject && !isHandsFull)
            {
                isHandsFull = true;
                unitAnim.SetBool("isMounted", true);
                mountObject.GetComponent<Interactable>().Interact();
                GameObject.Find("Character Manager").GetComponent<CharMgrScript>().SetCharacter(mountObject);
                mountObject.GetComponent<PlayableUnit>().isMounted = true;
                isMounted = true;
                this.GetComponent<Collider>().enabled = false;
                isHandsFull = true;
                Debug.Log("Mounting");
            }
        }

        // If the E button is pressed and there's nothing in your hands...
        if (Input.GetKeyDown(KeyCode.E) && !isHandsFull)
        {
            // Show an error if there's no object in range
            if (currentObject == null)
                throw new UnityException("No object in range");

            // If it's a switch, interact with it
            if (currentObject.GetComponent<Interactable>().type == Interactable.InteractableType.DoorSwitch)
                currentObject.GetComponent<Interactable>().Interact();

            // If it's a pickup, pick it up
            if (currentObject.GetComponent<Interactable>().type == Interactable.InteractableType.Pickup)
            {
                pickedUpObject = currentObject;
                pickedUpObject.transform.parent = playerPickUpSpace.transform;
                pickedUpObject.transform.localRotation = Quaternion.identity;
                pickedUpObject.transform.localPosition = new Vector3(0, 0, 0);
                pickedUpObject.GetComponent<Rigidbody>().isKinematic = true;
                //pickedUpObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                pickedUpObject.GetComponent<Rigidbody>().useGravity = false;
                //playerPickUpSpace.GetComponent<Collider>().isTrigger = false;
                pickedUpObject.GetComponent<Interactable>().interactableSfx.Play();
                isHandsFull = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.E) && isHandsFull)
        {
            // If your hands are full, drop the object
            DropObject();
            isHandsFull = false;
        }
    }

    // Drops the object in hand
    public void DropObject()
    {
        //playerPickUpSpace.GetComponent<Collider>().isTrigger = true;
        pickedUpObject.transform.parent = null;
        pickedUpObject.GetComponent<Rigidbody>().isKinematic = false;
        //pickedUpObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        pickedUpObject.GetComponent<Rigidbody>().useGravity = true;
        pickedUpObject = null;
    }

    // Collider trigger functions
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>() && other.gameObject.GetComponent<Interactable>().canInteract)
            {
                if(other.gameObject.tag == "Spider")
                 mountObject = other.gameObject;
                else currentObject = other.gameObject;
            }
    }
    
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>() && other.gameObject.GetComponent<Interactable>().canInteract)
            {
                if(other.gameObject.tag == "Spider")
                 mountObject = other.gameObject;
                else currentObject = other.gameObject;
            }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>() && other.gameObject.GetComponent<Interactable>().canInteract)
            {
                if(other.gameObject.tag == "Spider")
                 mountObject = null;
                else currentObject = null;
            }
    }
}
