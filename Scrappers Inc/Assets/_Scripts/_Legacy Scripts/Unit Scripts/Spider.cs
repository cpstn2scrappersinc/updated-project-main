﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : PlayableUnit {

    // Boolean value for the spider touching the ground
    public bool isGrounded;

    // Variables when it's close to a climbable ledge
    //public bool atLedge;
    //public LedgeScript ledgeScript;


    // Jump height magnitude
    float jumpMagnitude = 8.0f;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        unitAnim = GetComponentInChildren<Animator>();
    }
    
    public override void Move()
    {
        if (isGrounded)
            base.Move();
    }

    public override void ECommand()
    {
        base.ECommand();

        // Dismounts the player
        if(Input.GetKeyDown(KeyCode.F) && isMounted)
        {
            GameObject player = GameObject.FindWithTag("Player");
            GameObject.Find("Character Manager").GetComponent<CharMgrScript>().SetCharacter(player);
            player.transform.parent = null;
            player.transform.position = new Vector3(this.transform.position.x - 2, 1.5f, this.transform.position.z);
            player.GetComponent<Rigidbody>().isKinematic = false;
            player.GetComponent<Rigidbody>().useGravity = true;
            player.GetComponent<Player>().isMounted = false;
            player.GetComponent<Player>().isHandsFull = false;
            player.GetComponent<Player>().unitAnim.SetBool("isMounted", false);
            player.GetComponent<Collider>().enabled = true;
            isMounted = false;
            Debug.Log("Dismounting");
        }
    }

    public override void SpacebarCommand()
    {
        base.SpacebarCommand();

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            // Jumping force, affected by player mounting
            //unitAnim.SetTrigger("Jump");
            jumpMagnitude = (isMounted) ? 6.0f : 8.0f;

            Vector3 jumpForce = Vector3.up * jumpMagnitude;
            Vector3 forwardForce = this.transform.forward * 5;
            rb.AddForce(jumpForce + forwardForce, ForceMode.Impulse);
        }

    }

    /*
    // Collision detection. Checks if the spider is on the ground or not
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Terrain")
            isGrounded = true;
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Terrain")
            isGrounded = true;

    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Terrain")
            isGrounded = false;
    }


    // Trigger commands for ledges
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "LedgeCollider")
        {
            atLedge = true;
            ledgeScript = other.gameObject.GetComponent<LedgeScript>();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "LedgeCollider")
        {
            atLedge = true;
            ledgeScript = other.gameObject.GetComponent<LedgeScript>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "LedgeCollider")
        {
            atLedge = false;
            ledgeScript = null;
        }
    }
    */
}
