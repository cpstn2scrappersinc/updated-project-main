﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayableUnit : MonoBehaviour {

    // The unit's rigidbody
    public Rigidbody rb;

    // Unit speed variables
    public float runSpeed;
    public float walkSpeed;

    // Unit's current speed
    float curSpeed;

    // Transform where the unit places the object it's picked up (box, mounted player...)
    //public GameObject camPos;
    public GameObject playerPickUpSpace;
    public bool isHandsFull;

    // Boolean value for mounting
    public bool isMounted = false;

    // Current interactable object
    public GameObject currentObject;

    // Boolean value. Shows true when the unit reaches the end goal
    public bool isAtEnd;

    // The Unit's type
    public UnitType unitType;
    public enum UnitType
    {
        Player,
        Spider,
    }

    // Animator controller variables
    public Animator unitAnim;

    // Command for E key
    public virtual void ECommand()
    {
    }

    // Command for spacebar
    public virtual void SpacebarCommand()
    {

    }

    public virtual void UpdateMoveAnim(Vector3 dir)
    {
        if (dir.x == 0 && dir.y == 0)
            unitAnim.SetBool("isMoving", false);
        else
            unitAnim.SetBool("isMoving", true);
        
        unitAnim.SetFloat("velocityX", dir.x);
        unitAnim.SetFloat("velocityZ", dir.z);
    }

    // Movement script
    public virtual void Move()
    {
        // When you want to move real fast, hold shift
        bool running = Input.GetKey(KeyCode.LeftShift);

        // Directional input
        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");

        // Speed, depending on shift-button
        curSpeed = (running) ? runSpeed : walkSpeed;

        // Direction and speed calculation + normalized
		Vector3 movement = ((transform.forward * vAxis) + (transform.right * hAxis)) * curSpeed;
        Vector3 heading = Vector3.Normalize(movement);

        // Y-velocity clamp for gravity
        movement.y = rb.velocity.y;

        // Movement vector added
        rb.velocity = movement;

        // Direction for anim
        UpdateMoveAnim(heading);
    }
}
