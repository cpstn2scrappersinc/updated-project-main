﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeScript : MonoBehaviour {

    public GameObject ledgeTop;

    public void ClimbTop(GameObject unit)
    {
        unit.transform.position = ledgeTop.transform.position;
    }
}
