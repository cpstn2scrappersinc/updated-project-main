﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeScript : MonoBehaviour {

    public GameObject tree;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SphereExplosion")
        {
            Debug.Log("hit");
            tree.SetActive(true);
        }
    }
}
