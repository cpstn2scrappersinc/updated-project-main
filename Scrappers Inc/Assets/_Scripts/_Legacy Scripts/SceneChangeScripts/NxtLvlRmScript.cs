﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NxtLvlRmScript : MonoBehaviour {

	public GameObject sceneMgr;
	public List<GameObject> characters;
	public int nextSceneIndex;

	void CheckCharacters ()
	{
		if (characters.Count == 2) 
		{
			Debug.Log("Next scene loaded");
			sceneMgr.GetComponent<SceneMgrScript>().LoadScene(nextSceneIndex);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.GetComponent<Player> () || other.GetComponent<Spider> ()) 
		{
			characters.Add(other.gameObject);
			CheckCharacters ();
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.GetComponent<Player> () || other.GetComponent<Spider> ()) 
		{
			characters.Remove(other.gameObject);
		}
	}
}
