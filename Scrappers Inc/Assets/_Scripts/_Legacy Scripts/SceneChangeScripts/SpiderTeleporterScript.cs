﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderTeleporterScript : TeleporterBaseScript {

	public GameObject humanTeleporter;

	void OnTriggerEnter (Collider other)
	{
		if (other.GetComponent<Spider> ()) 
		{
			
			SwitchTeleporter(true);
			SwitchLights(true);
			humanTeleporter.GetComponent<HumanTeleporterScript>().CheckTeleporters();
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.GetComponent<Spider> ()) 
		{
			SwitchTeleporter(false);
			SwitchLights(false);
		}
	}
}
