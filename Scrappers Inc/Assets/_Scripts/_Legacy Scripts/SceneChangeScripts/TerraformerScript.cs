﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerraformerScript : MonoBehaviour
{
    public List<PressurePlate> floorSwitches = new List<PressurePlate>();
    public Camera oldCam;
    public Camera newCam;
    public GameObject sphere;
    public Vector3 camTargetPos;
    public float time;

    GameController gamCon;

    public bool isStarted = false;

    public AudioSource tfSfx;

    IEnumerator terraformCoroutine;

    // Use this for initialization
    void Start ()
    {
        gamCon = GameObject.Find("GameController").GetComponent<GameController>();
        terraformCoroutine = MoveCam(time);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (StartTerraformer() && !isStarted)
        {
            Debug.Log("STARTED");
            Terraform();
        }
	}

    // If the plates are being pressed on, start terraforming sequence
    bool StartTerraformer()
    {
        foreach (PressurePlate plate in floorSwitches)
            if (plate.isPressed == false)
                return false;

        return true;
    }


    // Moves the new camera backwards
    IEnumerator MoveCam (float time)
	{
		// Float values for timer
		float i = 0;
		float rate = 1 / time;

		//While i is counting down to the time...
		if (i < time) 
		{
			tfSfx.Play();
		}

        while (i < time)
        {
            // Countdown
            i += Time.deltaTime;

            // Move the camera to where you want it to go
            newCam.transform.position = Vector3.MoveTowards(newCam.transform.position, camTargetPos, rate);

            // Make the camera look towards the terraformer
            //newCam.transform.LookAt(this.transform);

            if (i > 2)
                sphere.transform.localScale += Vector3.one * rate * 50;

            yield return 0;
        }

        yield return new WaitForSeconds(1);
        gamCon.ChangeScene();
        StopCoroutine(terraformCoroutine);
    }

    // The terraformation script, which starts the cutscene
    void Terraform()
    {
        isStarted = true;
        SetCameras();
        //charMgr.StopAllCharAnims();
        StartCoroutine(terraformCoroutine);
    }

    // Activates the new camera and turns off the old
    void SetCameras()
    {
        oldCam.gameObject.SetActive(false);
        newCam.gameObject.SetActive(true);
    }
}
