﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollision : MonoBehaviour {

    public float minDist;
    public float maxDist;
    float smooth = 10.0f;
    Vector3 pivotDir;
    public float distance;

	// Use this for initialization
	void Awake ()
    {
        pivotDir = transform.localPosition.normalized;
        distance = transform.localPosition.magnitude;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 desiredCameraPos = transform.parent.TransformPoint(pivotDir * minDist);
        RaycastHit hit;

        Debug.DrawLine(transform.parent.position, desiredCameraPos, Color.green);

        if (Physics.Linecast(transform.parent.position, desiredCameraPos, out hit))
        {
            Debug.Log("HIT");
            distance = Mathf.Clamp(hit.distance, minDist, maxDist);
        }
        else distance = maxDist;

        transform.localPosition = Vector3.Lerp(transform.localPosition, pivotDir * distance, Time.deltaTime * smooth);
	}


}
