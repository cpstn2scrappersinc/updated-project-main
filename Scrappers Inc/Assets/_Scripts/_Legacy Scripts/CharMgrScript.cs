﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharMgrScript : MonoBehaviour {

    // List of playable characters and their UI images
	public List<GameObject> characters;
    public List<Image> activeCharImage;
    public GameObject CharUICanvas;

    // Boolean for character tele-switch
    public bool isTeleOn;
    public bool startTeleOn;
    public bool terraforming = false;

    // Current active/non-active character
    public GameObject activeCharacter;
    public GameObject nonActiveCharacter;

    //Script variables
    public CameraFollow camFollow;
    PlayableUnit curUnitScript;

    public AudioSource teleswapSfx;
    GameController gamCon;
    TerraformerScript terraformer;

    public AudioSource charSwitchSfx;

	// Use this for initialization
	void Start ()
	{
		// Set initial variables
		activeCharacter = characters [0];
		nonActiveCharacter = characters [1];
		SetActiveChrImage (0);

		if (startTeleOn == false)
			isTeleOn = false;
		else 
			isTeleOn = true;

        curUnitScript = activeCharacter.GetComponent<PlayableUnit>();
        camFollow = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<CameraFollow>();
        gamCon = GameObject.FindGameObjectWithTag("GameController").GetComponentInParent<GameController>();
        terraformer = GameObject.FindGameObjectWithTag("Terraformer").GetComponentInParent<TerraformerScript>();
    }

    // Update is called once per frame
    void Update () 
	{
        KeyboardCommands();

        if (characters.Count >= 1)
            SwitchCharacter ();
           

        if (Input.GetKeyDown(KeyCode.R))
            RCommand();

        //nonActiveCharacter.GetComponent<PlayableUnit>().unitAnim.SetBool("isMoving", false);
    }
    void FixedUpdate()
    {
        //if(!GameObject.FindWithTag("Terraformer").GetComponent<TerraformerScript>().isStarted)
            Physics();
    }

    // return true if all characters are at the end point
    public bool AllAtEnd()
    {
        return (characters[0].GetComponent<PlayableUnit>().isAtEnd && characters[1].GetComponent<PlayableUnit>().isAtEnd);
    }

    // Sets active character
    public void SetCharacter(GameObject character)
    {

        nonActiveCharacter = activeCharacter;
        activeCharacter = character;
        //camFollow.SetTarget(activeCharacter);
        nonActiveCharacter.GetComponent<PlayableUnit>().unitAnim.SetBool("isMoving", false);
    }

    // Switches characters via numeric keypad
    void SwitchCharacter ()
	{
        if (!activeCharacter.GetComponent<PlayableUnit>().isMounted)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && activeCharacter != characters[0])
            {
                Debug.Log("Controlling Player");

                SetActiveChrImage(0);
                SetCharacter(characters[0]);
				charSwitchSfx.Play();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && activeCharacter != characters[1])
            {
                if(characters[1] == null)
                    throw new UnityException("No Unit as of yet!");
                else
                {
                    Debug.Log("Controlling Spider");
                    SetCharacter(characters[1]);
                    SetActiveChrImage(1);
					charSwitchSfx.Play();
                }
            }
        }

        curUnitScript = activeCharacter.GetComponent<PlayableUnit>();
		
    }

    //Switches places with current active character and sets active character
    void RCommand()
    {
        // if the spider is not mounted and is on the ground

        if (isTeleOn && GameObject.FindWithTag("Spider").GetComponent<Spider>().isGrounded 
            && !GameObject.FindWithTag("Spider").GetComponent<Spider>().isMounted)
        {
            Vector3 nActivePos = nonActiveCharacter.transform.position;
            Vector3 oldPos = activeCharacter.transform.position;

            activeCharacter.transform.position = new Vector3(nActivePos.x, nActivePos.y + 1.0f, nActivePos.z);
            nonActiveCharacter.transform.position = new Vector3(oldPos.x, oldPos.y + 1.0f, oldPos.z);

			teleswapSfx.Play();
            //SetCharacter(nonActiveCharacter);
        }
    }

    // Interaction Keyboard commands
    void KeyboardCommands()
    {
        curUnitScript.ECommand();
    }

    // Physics keyboard commands
    void Physics()
    {
        if (!gamCon.MenuUI.activeSelf && !terraformer.isStarted)
        {
            curUnitScript.Move();
            curUnitScript.SpacebarCommand();
        }
    }

    // Turns on the Character UI
    public void ActivateCharUI()
    {
        CharUICanvas.SetActive(true);
        SetActiveChrImage(0);
    }

    // Sets the specified character image UI as active
    void SetActiveChrImage(int keyCommand)
    {
        foreach (Image image in activeCharImage)
        {
            image.GetComponent<CanvasRenderer>().SetAlpha(0.25f);
            image.color = Color.white;
        }

        activeCharImage[keyCommand].GetComponent<CanvasRenderer>().SetAlpha(1);
        activeCharImage[keyCommand].color = Color.green;
    }

    public void StopAllCharAnims()
    {
        foreach(GameObject unit in characters)
            unit.GetComponent<PlayableUnit>().unitAnim.SetBool("isMoving", false);
    }
}
