﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatModifier
{
	// The order in which the stat modifier is read
	public readonly int statOrder;

	// Number value of the stat
	public readonly float statVal;

	// Type of modifier
	public readonly StatModType statType;

	// Where the modifier comes from
	public readonly object statSource;

	// Constructor
	public StatModifier(float val, StatModType type, int order, object source)
	{
		statVal = val;
		statType = type;
		statOrder = order;
		statSource = source;
	}

	// Requires Value and Type. Calls the "Main" constructor and sets Order and Source to their default values: (int)type and null, respectively.
	public StatModifier(float value, StatModType type) : this(value, type, (int)type, null) { }

	// Requires Value, Type and Order. Sets Source to its default value: null
	public StatModifier(float value, StatModType type, int order) : this(value, type, order, null) { }

	// Requires Value, Type and Source. Sets Order to its default value: (int)Type
	public StatModifier(float value, StatModType type, object source) : this(value, type, (int)type, source) { }
}

//Stat types, whether they are flat or a percentage change
public enum StatModType
{
	Flat = 100,
	PercentAdd = 200,
	PercentMult = 300,
}
