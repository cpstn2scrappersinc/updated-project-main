﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketScript : Puzzle
{
    // Types of contents for the bucket
    public enum BucketContent
    {
        None,
        Water,
        Manure,
    }

    // The current content of the bucket
    public BucketContent content;

    // Water level for bucket
    public float curWaterLevel = 0.0f;

    // Is the player holding on to this?
    public bool isHeld = false;

	// Use this for initialization
	void Start ()
    {
        type = PuzzleType.Pickup;
        content = BucketContent.None;
	}

    private void Update()
    {
        curWaterLevel = waterClamp();
    }

    // Empties the bucket of its contents
    public void EmptyBucket()
    {
        curWaterLevel = 0.0f;
        content = BucketContent.None;
    }

    // Clamps the water level to prevent it from going over 100
    float waterClamp()
    {
        float curWater = curWaterLevel;

        if (curWater > 100)
            curWater = 100;

        return curWater;
    }
}
