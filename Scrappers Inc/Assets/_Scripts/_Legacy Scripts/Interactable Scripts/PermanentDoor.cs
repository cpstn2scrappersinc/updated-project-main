﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PermanentDoor : Interactable
{
    bool isOn = false;

    public override void Interact()
    {
        if (!isOn)
        {
            this.gameObject.SetActive(!this.gameObject.activeSelf);
            isOn = true;
        }
    }
}
