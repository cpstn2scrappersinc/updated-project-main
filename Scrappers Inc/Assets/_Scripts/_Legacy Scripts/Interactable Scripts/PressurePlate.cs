﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PressurePlate : Interactable {

    // Initial transform of the switch/button
    Vector3 initialTransform;

    // List of objects (used for platforms)
    public List<Interactable> interactableObjects;

    // Boolean if the button/plate is being pressed down
    public bool isPressed;

	// Use this for initialization
	void Start ()
    {
        isPressed = false;
        initialTransform = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        LightsOn();

        // If the button is being pressed, interact with all stored objects
        if (isPressed)
        {
            foreach (Interactable interactable in interactableObjects)
                interactable.Interact();
        }

        // If there is a linked object and it isnt a permanent door
        if(linkedObject && !linkedObject.GetComponent<PermanentDoor>())
        {
            // If it is a retractable door...
            if (linkedObject.GetComponent<Door>())
                linkedObject.GetComponent<Door>().isToggled = isPressed;
            else linkedObject.SetActive(!isPressed);
        }
    }

    // Collider trigger functions
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Pickup>() || other.gameObject.GetComponent<PlayableUnit>())
        {
			interactableSfx.Play();

            // Button goes down
            transform.position = initialTransform - new Vector3(0, 0.05f, 0);

            // Bool becomes true
            lightsOn = true;
            isPressed = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Pickup>() || other.gameObject.GetComponent<PlayableUnit>())
        {
            // Stays down
            transform.position = initialTransform - new Vector3(0, 0.05f, 0);

            // Still true
            lightsOn = true;
            isPressed = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Pickup>() || other.gameObject.GetComponent<PlayableUnit>())
        {
            // Returns to "on" mode/goes back to original position
            transform.position = initialTransform;

            // Bool now false again
            lightsOn = false;
            isPressed = false;
        }
    }
}
