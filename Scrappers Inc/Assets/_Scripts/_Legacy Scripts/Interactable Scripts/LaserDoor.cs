﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDoor : MonoBehaviour {

    public List<LaserReceiverScript> receivers = new List<LaserReceiverScript>();
    public GameObject door;

    // Update is called once per frame
    void Update()
    {
        if (CheckDoors())
            door.SetActive(false);
        else door.SetActive(true);
    }

    // Checks if all the receivers are being hit by the laser
    bool CheckDoors()
    {
        foreach (LaserReceiverScript activeReceiver in receivers)
            if (!activeReceiver.isHit)
                return false;

        return true;
    }
}
