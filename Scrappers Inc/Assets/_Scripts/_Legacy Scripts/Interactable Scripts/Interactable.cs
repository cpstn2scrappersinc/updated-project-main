﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactable : MonoBehaviour
{
    // Objects linked to the interactable (AKA doors)
    public GameObject linkedObject;
    public List<GameObject> guideLights = new List<GameObject>();
    public bool lightsOn = false;

    // What kind of interactable is it?
    public InteractableType type;

    public AudioSource interactableSfx;

    public enum InteractableType
    {
        DoorSwitch,
        PressurePlate,
        Pickup,
        Mount,
    }

    public bool canInteract = false;

    public Canvas helpSigns;
    public GameObject activeChar;

	// Use this for initialization
	void Start ()
    {       

    }
	
    public virtual void Init()
    {
        helpSigns = GetComponentInChildren<Canvas>();
    }

	// Update is called once per frame
	void Update ()
    {
        if (helpSigns != null)
            ShowHelpSigns();
    }

    // Main interaction function
    public virtual void Interact()
    {
    }

    public virtual void ShowHelpSigns()
    {
        activeChar = GameObject.Find("GameController").GetComponentInChildren<CharMgrScript>().activeCharacter;
        CanvasGroup canvasRend = helpSigns.GetComponent<CanvasGroup>();
        Vector3 distanceBetween = activeChar.transform.position - transform.position;

        helpSigns.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);

        if (distanceBetween.magnitude > 5.0f)
            canvasRend.alpha = 0;
        else canvasRend.alpha = 1 /  distanceBetween.magnitude*2f;
    }

    public virtual void LightsOn()
    {
        foreach(GameObject light in guideLights)
        {
            light.SetActive(lightsOn);
        }
    }
}
