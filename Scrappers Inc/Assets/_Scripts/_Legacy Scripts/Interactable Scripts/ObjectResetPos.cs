﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectResetPos : MonoBehaviour {

	private Vector3 startingPos;

	void Start ()
	{
		startingPos = this.transform.position;
	}

	void ResetPos ()
	{
		this.transform.position = startingPos;
		this.transform.rotation = Quaternion.identity;
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Abyss>())
		{
			ResetPos();
		}
	}
}
