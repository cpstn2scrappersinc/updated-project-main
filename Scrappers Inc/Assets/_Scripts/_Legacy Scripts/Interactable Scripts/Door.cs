﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Interactable
{
    public bool isToggled;

    private Vector3 originalPos;
    public Vector3 targetPos;
    private float speed;

    private Vector3 currentPos;

    private void Start()
    {
        // Set variables
        //isToggled = false;

        originalPos = this.transform.localPosition;

        if(targetPos == Vector3.zero)
            targetPos = new Vector3(transform.localPosition.x, transform.localPosition.y - 2.0f, transform.localPosition.z);

        currentPos = originalPos;

        speed = 6.0f;
    }

    void Update()
    {
        // Ensures the door is working
        if (!isToggled)
            Close();
        else Open();

        DoorAnim(currentPos);
    }

    public override void Interact()
    {
        base.Interact();

        // Toggles the switch on or off
        isToggled = !isToggled;
    }

    private void Open()
    {
        // Makes the door passable
        gameObject.GetComponent<Collider>().enabled = false;

        // Raises the door
        currentPos = targetPos;
    }

    private void Close()
    {
        // Makes the door impassable
        gameObject.GetComponent<Collider>().enabled = true;

        // Closes the door
        currentPos = originalPos;
    }

    void DoorAnim(Vector3 direction)
    {
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, direction, speed * Time.deltaTime);
    }

}
