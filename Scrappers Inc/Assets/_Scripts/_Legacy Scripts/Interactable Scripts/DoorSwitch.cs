﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorSwitch : Interactable
{

    public bool isTimedSwitch;
    public bool isOn;

	// Use this for initialization
	void Start ()
    {
        canInteract = true;
        helpSigns = GetComponentInChildren<Canvas>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        LightsOn();
    }

    public override void Interact ()
	{
		base.Interact ();

        // Plays the audio file
		interactableSfx.Play ();

        // If it's a timed switch, turn it on
		if (isTimedSwitch)
			isOn = true;
		else 
		{
            // OLD CODE
			if (linkedObject.GetComponent<Retractable> ()) 
				linkedObject.GetComponent<Retractable> ().ToggleRetraction ();
			else 
				linkedObject.GetComponent<Interactable> ().Interact ();
        }

        // Turns the guide lights on or off
        lightsOn = !lightsOn;
    }
}
