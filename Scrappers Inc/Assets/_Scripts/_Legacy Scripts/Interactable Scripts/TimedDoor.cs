﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDoor : MonoBehaviour {

    public List<DoorSwitch> switches = new List<DoorSwitch>();
    public List<GameObject> doors = new List<GameObject>();
    float timer = 0;
    float soundTimer = 0;

    public AudioSource timerSfx;

    // Update is called once per frame
    void Update ()
    {
        if (CheckDoors())
            OpenDoor();
	}

    // Checks if all the switches are turned on
    bool CheckDoors()
    {
        foreach (DoorSwitch activeSwitch in switches)
            if (!activeSwitch.isOn)
                return false;

        return true;
    }

    // Opens the door for a set amount of time
    void OpenDoor ()
	{
		foreach (GameObject door in doors)
			door.SetActive (false);

		timer += Time.deltaTime;
        soundTimer += Time.deltaTime;

        if (soundTimer >= 1.0f && timer <= 4.0f)
        {
            timerSfx.Play();
            soundTimer = 0;
        }

        if (timer >= 4.0f)
        {

            foreach (DoorSwitch activeSwitch in switches)
            {
                activeSwitch.lightsOn = false;
                if (activeSwitch.isOn)
                    activeSwitch.isOn = false;
            }

            foreach (GameObject door in doors)
                door.SetActive(true);

            timer = 0;
        }
    }
}
