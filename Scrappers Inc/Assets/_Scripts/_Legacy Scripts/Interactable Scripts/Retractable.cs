﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Retractable : MonoBehaviour {

    private bool isRectracted;
    private bool opened;
    private bool closed;

    private Vector3 originalPos;
    private Vector3 targetPos;
    private float speed;

    private Renderer rend;

    private void Start()
    {
        isRectracted = false;
        opened = false;
        closed = true;
        originalPos = this.transform.localPosition;
        targetPos = new Vector3(transform.localPosition.x, transform.localPosition.y - 1.0f, transform.localPosition.z);
        speed = 3.0f;

        rend = GetComponent<MeshRenderer>();
        //rend.enabled = true;
    }

    void Update()
    {
        
    }

    public void ToggleRetraction ()
	{
		if (isRectracted) 
			Close ();
		else Open ();

        isRectracted = !isRectracted;
    }

    private void Open()
    {
        PlayOpenAnim();                                         // Plays animation 
        gameObject.GetComponent<BoxCollider>().enabled = false; // before disabling collider
        rend.enabled = false;
    }

    private void Close()
    {
        gameObject.GetComponent<BoxCollider>().enabled = true;  // Plays animation 
        PlayCloseAnim();                                        // before reenabling collider
        rend.enabled = true;
    }

    void PlayOpenAnim()
    {
        if(isRectracted && !opened)
        {
            transform.position = Vector3.MoveTowards(transform.localPosition, targetPos, speed * Time.deltaTime);
            closed = false;
        }
    }

    void PlayCloseAnim()
    {
        if (!isRectracted && !closed)
        {
            transform.position = Vector3.MoveTowards(transform.localPosition, originalPos, speed * Time.deltaTime);
            opened = false;
        }
    }

}
