﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceScript : MonoBehaviour
{
    // The type of resource the object gives the player
    public enum ResourceType
    {
        Water,
        Manure,
    }
    // The current type
    public ResourceType type;
    public float waterValue = 25.0f;

    NewPlayerScript player;
    public GameObject waterPrompt;
    Camera cam;

    public GameObject gameCtrlr;

    private void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<NewPlayerScript>();
        cam = Camera.main;
    }

    private void Update ()
	{
		// Billboard effect
		waterPrompt.transform.LookAt (waterPrompt.transform.position + cam.transform.rotation * Vector3.forward,
			cam.transform.rotation * Vector3.up);

		if (gameCtrlr.GetComponent<GameControllerScript> ().gameTimer == 0) 
		{
			//waterPrompt.SetActive (player.nearWater);
		}
    }

    // When the player enters/stays within the object's collider, then...
    private void OnTriggerEnter(Collider other)
    {
        // If this is a water source and the game object entering the collider is a player ...
        if (this.type == ResourceType.Water)
        {
            if (other.gameObject.CompareTag("Player"))
                other.gameObject.GetComponent<NewPlayerScript>().nearWater = true;   // The player is now near the water
        }
    }

    private void OnTriggerStay(Collider other)
    {
        // If this is a water source and the game object entering the collider is a player ...
        if (this.type == ResourceType.Water)
        {
            if (other.gameObject.CompareTag("Player"))
                other.gameObject.GetComponent<NewPlayerScript>().nearWater = true;   // The player is now near the water
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // If this is a water source and the game object entering the collider is a player ...
        if (this.type == ResourceType.Water)
        {
            if (other.gameObject.CompareTag("Player"))
                other.gameObject.GetComponent<NewPlayerScript>().nearWater = false;   // The player is now far from the water
        }
    }
}
