﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoatManager : MonoBehaviour
{

    public List<PackGoatScript> goats = new List<PackGoatScript>();
	
	// Update is called once per frame
	void Update ()
    {
        //Removes null values
        for (int i = 0; i < goats.Count; i++)
        {
            if (goats[i] == null)
            {
                goats.Remove(goats[i]);
                break;
            }
        }
    }

    // Sends the first goat on the list on its merry way
    public void SendGoatOff()
    {
        goats[0].isFulfilled = true;
        goats[0].isEmptyHanded = false;
        goats.Remove(goats[0]);
    }

    // Sends the goat off sans gains
    public void SendGoatEmpty()
    {
        goats[0].isFulfilled = true;
        goats.Remove(goats[0]);
    }
}
