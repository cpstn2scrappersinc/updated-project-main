﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainTreeScript : MonoBehaviour
{
    // Growth timer values
    float maxGrowthTime = 0.0f;
    public float curGrowthTime = 0.0f;
    public GameObject growthCanvas;
    public Image growthMeter;
    public int[] growthTimes;

    // Growth stages for the attached tree
    public bool isPlanted = false;
    public GameObject[] growthStages;
    public int stage = 0;
    GameObject tree;

    //Score values by lim
    public int[] scoreRewards;

    // Misc refs
    public Camera cam;
    public GameObject pointLight;
    public GameObject arrow;
    NewPlayerScript playerScript;
    ItemDatabase itemDB;
   
    TreeWaterScript waterScript;
    TreeFruitScript fruitScript;

    //public GameObject skillsCanvas;
	GameControllerScript gamCon;
    public GameObject sfxMgr;
    public GameObject tutMgr;
    public GameObject scoreMgr;

    public bool secondTree;

    private void Start()
    {
		//skillsCanvas = GameObject.Find("SeedUI");
		scoreMgr = GameObject.Find("ScoreManager");
		tutMgr = GameObject.Find("Tutorial Manager");
    	sfxMgr = GameObject.FindWithTag("SfxMgr");
        playerScript = GameObject.FindWithTag("Player").GetComponent<NewPlayerScript>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();

        cam = Camera.main;
        
        waterScript = GetComponent<TreeWaterScript>();
        fruitScript = GetComponent<TreeFruitScript>();

    }

    private void Update()
    {
        // Turns the object's point light on if the player is nearby
        pointLight.SetActive((playerScript.nearbySoil == this.gameObject) ? true : false);
		arrow.SetActive((playerScript.nearbySoil == this.gameObject) ? true : false);

        // Water and Growth functions
        Growth();
        //SpawnAFruit();

        //if(!gamCon.isTutorialDone)
        //    TutorialEnd();
    }

    // Tutorial now ends via newtutorialmgrscript
    /*void TutorialEnd()
    {
        if (fruitScript.fruit == itemDB.itemDatabase[0] && stage >= 4)
        {
           gamCon.ShowBar();
           gamCon.isTutorialDone = true;
        }
    }*/

    // Sets the BG for the fruit production UI depending on the seed
    void FruitProdBGSet(Seed seed)
    {
        switch(seed.itemName)
        {
            case "Banana Seed":
                fruitScript.fruitProdBG.sprite = fruitScript.fruitProdImage[1];
                break;

            case "Mango Seed":
                fruitScript.fruitProdBG.sprite = fruitScript.fruitProdImage[2];
                break;

            default:
                fruitScript.fruitProdBG.sprite = fruitScript.fruitProdImage[0];
                break;
        }
    }

    // Function to determine growth
    void Growth ()
	{
		// If the tree has been watered...
		if (waterScript.isWatered && stage < 4)
        {
			// Start ticking down the growth timer
			// Insert Gathering tutorial here

			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 5) //Inserted fruit gathering here
                tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(5);
            
            curGrowthTime += 1 * Time.deltaTime;

            // When the timer reaches max...
            if(curGrowthTime > maxGrowthTime)
            {
                // Reset the timer and increase the time to grow for the next stage
                waterScript.isWatered = false;
                maxGrowthTime = growthTimes[stage];

                curGrowthTime = 0;
            }
        }

		else if (stage >= 4)
        {
			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 8)
                tutMgr.GetComponent<NewTutorialMgrScript>().tutorialMsgsA[6].SetActive(false);
			//if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 13)
			//	tutMgr.GetComponent<NewTutorialMgrScript>().tutorialMsgsA[10].SetActive(false);
			
        }

        // If the water level is neither at 0 nor at max
        if (curGrowthTime > 0 && curGrowthTime < maxGrowthTime)
        {
            // Turn the slider on
            growthCanvas.SetActive(true);

            // Sets the water meter for the mound
            growthMeter.fillAmount = curGrowthTime / maxGrowthTime;
        }
        else growthCanvas.SetActive(false);   // Turn the slider off

        // Billboard effect
        growthCanvas.transform.LookAt(growthCanvas.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);
    }

    // Spawns the tree when the conditions are met
    public void SpawnTree()
    {
        // Spawns a tree
        if (!tree)
            tree = Instantiate(growthStages[stage], this.transform);
        else
        {
            // Remove the old tree and put a new one up
            Destroy(tree);
            tree = Instantiate(growthStages[stage], this.transform);
        }
		scoreMgr.GetComponent<ScoreMgrScript>().IncreaseScore(scoreRewards[stage]);
		Debug.Log("Player received " + scoreRewards[stage] + " score."); //increases score every time tree grows
    }
    
    // ================================================================== Button//Public functions ==================================================================

    // Plants a tree, setting its growth stages and ticking the boolean on
    public void PlantTree(Seed seed)
    {
        isPlanted = true;
        growthStages = seed.treeStages;
        scoreRewards = seed.scoreRewards; //added by lim
        fruitScript.fruit = seed.fruit;
        growthTimes = seed.growthTimes;
        maxGrowthTime = growthTimes[0];
        FruitProdBGSet(seed);
    }


    // ================================================================== Trigger functions ==================================================================


    // Trigger detection
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<NewPlayerScript>().nearbySoil = this.gameObject;
			other.gameObject.GetComponent<NewPlayerScript>().nearSoil = true;
			other.gameObject.GetComponent<SkillsScript>().TransparentSkillUI(false);

			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 6)
				tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(6);
    		
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
			other.gameObject.GetComponent<NewPlayerScript>().nearbySoil = this.gameObject;
			other.gameObject.GetComponent<NewPlayerScript>().nearSoil = true;
			other.gameObject.GetComponent<SkillsScript>().TransparentSkillUI(false);
		}
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<NewPlayerScript>().nearbySoil = null;
			other.gameObject.GetComponent<NewPlayerScript>().nearSoil = false;
			other.gameObject.GetComponent<SkillsScript>().TransparentSkillUI(true);

        }
    }
}
