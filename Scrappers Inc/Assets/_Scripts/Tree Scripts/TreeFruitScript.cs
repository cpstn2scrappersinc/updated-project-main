﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreeFruitScript : MonoBehaviour
{
    // Fruit spawning
    public GameObject fruit;
    public float curFruitTime = 0.0f;
    public float fruitWateringTime = 0.0f;
    public int maxFruits;
    public int curFruits;
    public bool isFertilised = false;
	public GameObject flashMgr;

    // UI Elements
    public GameObject fruitUI, fruitProdUI, fruitProdButton;
    public Image fertiliserMe;
    public Sprite[] fruitProdImage;
    public Image fruitProdBG, fruitProdBar;

    // Misc
    MainTreeScript treeScript;
    TreeWaterScript waterScript;
    ItemDatabase itemDB;
    InventorySystem invSystem;
	public GameObject tutMgr;

    // Start is called before the first frame update
    void Start()
    {
		tutMgr = GameObject.Find("Tutorial Manager");
		flashMgr = GameObject.Find("UIFlashMgr");
        treeScript = GetComponent<MainTreeScript>();
        waterScript = GetComponent<TreeWaterScript>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        invSystem = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
    }

    // Update is called once per frame
    void Update()
    {
        SpawnAFruit();
        FruitUI();
    }
    
    // Boolean value to decide whether or not the tree produces extra fruits
    bool ExtraFruits()
    {
        if (fruit == itemDB.itemDatabase[1] || fruit == itemDB.itemDatabase[3])
            return true;
        else return false;
    }

    // Manages the UI for fruit production
    void FruitUI()
    {
        // Billboard effect
        fruitUI.transform.LookAt(fruitUI.transform.position + treeScript.cam.transform.rotation * Vector3.forward,
            treeScript.cam.transform.rotation * Vector3.up);

        //Activate the fruit prod UI when a fruit is being made
        fruitProdUI.SetActive(curFruitTime > 0);

        // Show the prompt for acquiring fruits
        fruitProdButton.SetActive(curFruits > 0);
    }

    // Spawns fruits when the tree is matured
    void SpawnAFruit()
    {
        // If the plant has matured, and is watered, and has yet to spawn the max number of fruits...
        if (treeScript.stage >= 4 && waterScript.isWatered && curFruits <= maxFruits)
        {
			
            // Shows whether or not the plant needs to be fertilised
           
            fertiliserMe.gameObject.SetActive(isFertilised);

            // If it's fertilised, halve the time for fruits
            float fruitTime = (isFertilised) ? treeScript.growthTimes[4] * 0.75f : treeScript.growthTimes[4];

            // Tick the timer down
            curFruitTime += 1 * Time.deltaTime;
            fruitProdBar.fillAmount = curFruitTime / fruitTime;

            // Once the timer for spawning is reached
            if (curFruitTime > fruitTime)
            {
                // Add two fruits if the tree can, else add one
                curFruits += (ExtraFruits()) ? 2 : 1;


				if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 8) //Inserted fruit gathering tutorial here
					tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(8);

				
                            	
                // Reset the timer
                curFruitTime = 0;
            }
        }
    }

    public IEnumerator RemoveFertilization() // Fertilization buff duration
    {
    	yield return new WaitForSeconds(18);
    	isFertilised = false;
    }

    // ================================================================== Button//Public functions ==================================================================

    public void AddFruit()
    {
        // If the player's inventory is not full...
        if (!invSystem.InventoryFull())
        {
            Item itemScript = fruit.GetComponent<Item>();

            // For each slot in the inventory...
            for (int i = 0; i < invSystem.slots.Length; i++)
            {
                // If this slot (slot[i]) is not filled yet...
                if (!invSystem.slots[i].isFull)
                {

                    // Set it to true / it is now filled
                    invSystem.slots[i].isFull = true;

                    // Adds a button corresponding to the item added in the inventory
                    GameObject itemBtnObj = Instantiate(itemScript.itemButton, invSystem.slots[i].transform, false);
					GameObject itemBtnFx = Instantiate(itemScript.acquiredFx, invSystem.slots[i].transform, false); // the fx upon retrieving item

					if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 9)
					{
               	 		tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(9);
						
            		}
					else if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 13)
					{
						tutMgr.GetComponent<NewTutorialMgrScript>().coconutIcon = itemBtnObj;
						tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(13);
						tutMgr.GetComponent<NewTutorialMgrScript>().tutorialMsgsA[10].SetActive(false);
					}
                    //set tutmanagers coconut here

                    // Add the gameobject to the inventory
                    invSystem.playerInventory[i] = fruit;

                    // Deduct the fruit from the tree
                    curFruits--;

					flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashGreen(3));



                    // Stop the loop
                    break;
                }
            }
        }
        else Debug.Log("No space!");
    }
}
