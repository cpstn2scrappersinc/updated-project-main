﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreeWaterScript : MonoBehaviour
{
    // Levels of water this plant needs + currently has + UI elements
    float maxWaterLvl = 2.0f;
    public float curWaterLvl = 0.0f;
    public bool isWatered = false;
    public float fruitWateringTime = 0.0f;

    // UI Elements
    public GameObject waterCanvas;
    public Image waterMeter;
    public Image waterMe;

    // Misc
    MainTreeScript treeScript;
    NewPlayerScript playerScript;
    public bool mouseOver = false;

    public bool scoreGranted;

	public GameObject scoreMgr;

    // Start is called before the first frame update
    void Start()
    {
		scoreMgr = GameObject.Find("ScoreManager");
        treeScript = GetComponent<MainTreeScript>();
        playerScript = GameObject.Find("Player").GetComponent<NewPlayerScript>();

    }

    // Update is called once per frame
    void Update()
    {
        WaterUI();
        Water();
    }

    // Function to manage water-based UI
    void WaterUI()
    {
        // Billboard effect
        waterCanvas.transform.LookAt(waterCanvas.transform.position + treeScript.cam.transform.rotation * Vector3.forward,
            treeScript.cam.transform.rotation * Vector3.up);

        // Activates the prompt if the mound is seeded + needs to be watered
        waterMe.gameObject.SetActive(treeScript.isPlanted && !isWatered);

        // Sets the water meter for the mound
        waterMeter.gameObject.SetActive(!isWatered && curWaterLvl > 0 && curWaterLvl < maxWaterLvl);
        waterMeter.fillAmount = curWaterLvl / maxWaterLvl;
    }

    // Function to manage water UI
    void Water()
    {
        // If the plant is watered to max...
        if (curWaterLvl > maxWaterLvl)
        {
            // Reset water values, increment the max, increase the stage and set isWatered to true
            isWatered = true;

			if(scoreGranted == false && treeScript.stage == 4)
            {
				scoreMgr.GetComponent<ScoreMgrScript>().IncreaseScore(GetComponent<MainTreeScript>().scoreRewards[0] / 2);
				scoreGranted = true;
            }

            // If it's not fully grown yet...
            if (treeScript.stage < 4)
            {
                // Spawn a new tree, increase
                treeScript.SpawnTree();
                treeScript.stage++;

                // sound effects - lim
                if (treeScript.stage == 0)
                    treeScript.sfxMgr.GetComponent<SfxMgrScript>().PlaySfx(4);

                else if (treeScript.stage == 1)
                    treeScript.sfxMgr.GetComponent<SfxMgrScript>().PlaySfx(5);

                else if (treeScript.stage == 2)
                    treeScript.sfxMgr.GetComponent<SfxMgrScript>().PlaySfx(6);
                else
                    treeScript.sfxMgr.GetComponent<SfxMgrScript>().PlaySfx(7);

                //maxWaterLvl += 2;
                curWaterLvl = 0;
            }

        }

        // If the tree has matured + is watered...
        if (treeScript.stage >= 4 && isWatered)
        {
            // Count down a timer
            fruitWateringTime += 1 * Time.deltaTime;

            // When the timer reaches its limit, the tree needs watering again
            if (fruitWateringTime > ((treeScript.growthTimes[treeScript.stage]) * 2) + 1)
            {
                fruitWateringTime = 0;
                curWaterLvl = 0;
                isWatered = false;

				scoreGranted = false;
            }
        }
    }

    //// Trigger detection
    //private void OnTriggerEnter(Collider other)
    //{
    //    if(other.tag == "WaterSpray")
    //            if (treeScript.isPlanted && !isWatered)
    //        curWaterLvl += 1.0f * Time.deltaTime;
    //}
    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.tag == "WaterSpray")
    //        if (treeScript.isPlanted && !isWatered)
    //            curWaterLvl += 1.0f * Time.deltaTime;
    //}

    private void OnMouseOver()
    {
        mouseOver = true;
    }
    private void OnMouseExit()
    {
        mouseOver = false;
    }
}
