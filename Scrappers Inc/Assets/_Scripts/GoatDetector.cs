﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoatDetector : MonoBehaviour
{
    // Timer and bool value for spawning manure
    public float timer;
    public bool hasSpawned;

    // Goat's droppings and butt
    public GameObject manure;
    public Transform rearEnd;

    public GameObject sfxMgr;
    public GameObject tipsMgr;

    void Start()
    {
		sfxMgr =  GameObject.FindWithTag("SfxMgr");
		tipsMgr =  GameObject.FindWithTag("TipMgr");
    }

    private void Update()
    {
        // If the goat has recently been fed/spawned manure...
        if(hasSpawned)
        {
            // Start a timer
            timer += Time.deltaTime;

            // When the timer reaches above 10s, make it available to spawn manure again
            if (timer >= 10.0f)
                hasSpawned = false;
        }
    }

    // Spawns manure
    public void SpawnManure()
    {
        Instantiate(manure, rearEnd.position, Quaternion.identity);
        hasSpawned = true;

        sfxMgr.GetComponent<SfxMgrScript>().PlaySfx(2);
    }

    // Determines whether or not the player is in the collider
    private void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.GetComponent<NewPlayerScript> ()) 
		{
			tipsMgr.GetComponent<TipsMgr>().StartCoroutine("ShowGoatTip");
			other.gameObject.GetComponent<NewPlayerScript> ().nearbyGoat = this.gameObject;
		}
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearbyGoat = this.gameObject;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearbyGoat = null;
    }
}
