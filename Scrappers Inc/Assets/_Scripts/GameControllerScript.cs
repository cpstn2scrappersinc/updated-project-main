﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControllerScript : MonoBehaviour
{
    bool youLost = false;
    public bool isTutorialDone = false;

    // Variables and UI elements for sustenance mechanic
    public GameObject sustenanceCanvas;
    public Image sustenanceBar;
    public Text sustenanceUIText;
    bool IsTimerSpeedingUp = false;
    public float sustenanceTimerSpeed = 2;
    public float curSustenanceTimer = 0;
    float maxSustenanceTimer = 100;
    public float sustenanceSpeedTimer = 0;

    // Variables for orders, submissions and goats
    public GameObject orderNoticeboard;
    public List<Orders> orderList = new List<Orders>();
    public Text orderListText;
    public float orderTimer;
    public GameObject orderPrefab;
    //public GameObject goatPrefab;
    //public Transform goatSpawn;
    //public GameObject goatFirstLoc, goatSecondLoc;
    public GameObject riceGoatPrefab;
    public Transform riceGoatSpawn;
    float riceGoatTimer;
    public List<GameObject> riceGoatList = new List<GameObject>();
    public GameObject riceGoatFirstLoc, riceGoatSecondLoc;

    // Variables for drought and flood phases
    public bool isHalfTime = false;
    public bool isDroughtPhase = true;
    public float gameTimer = 0;
    public float phaseTimer = 0;
    public GameObject clockUICanvas;
    public Image clockUI;
    int phaseIncrement1, phaseIncrement2 = 0;
    public GameObject rain;

    public int minOrderRange, maxOrderRange = 0;

    // Main Menu stuff
    public GameObject gameOverScreen;
    public GameObject winScreen;

    // Script and GameObject Refs
    ItemDatabase itemDB;
    GameObject player;
    InventorySystem invSystem;
    GoatManager goatManager;

	public GameObject sfxManager;
	public GameObject tipsMgr;

	public GameObject tutMgr;

	public GameObject halfTimeNotif;

	public GameObject lowFoodNotif;

    //NewPlayerScript playerScript;

    private void Start()
    {
        // Item references
		tutMgr = GameObject.Find("Tutorial Manager");
        itemDB = GetComponent<ItemDatabase>();
        player = GameObject.FindWithTag("Player");
        //playerScript = player.GetComponent<NewPlayerScript>();
        invSystem = player.GetComponent<InventorySystem>();
        goatManager = GameObject.FindWithTag("Submissions").GetComponent<GoatManager>();
    }

    private void Update ()
	{
		// Drought function
		SustenanceCountdown ();
		OrdersAndSubmissions ();

		if (isTutorialDone)
			Phases ();

		if (!isDroughtPhase) 
		{
			//tipsMgr.GetComponent<TipsMgr>().StartCoroutine("ShowRiceTip");
			//SpawnRiceGoats ();
		}

		if(curSustenanceTimer >= 75)
			lowFoodNotif.SetActive(true);
		else
			lowFoodNotif.SetActive(false);
		
    }

    // Function to control sustenance mechanic
    void SustenanceCountdown()
    {
        // Ends the game if the player reaches max limit
        if(curSustenanceTimer >= maxSustenanceTimer)
            youLost = true;

        if(youLost)
        {
            gameOverScreen.SetActive(true);
            gameOverScreen.GetComponent<CanvasGroup>().alpha += 0.2f * Time.deltaTime;
        }

        //Wins the game if the player achieves 8 mins of pain
        if (gameTimer > 480)
        {
            winScreen.SetActive(true);
            winScreen.GetComponent<CanvasGroup>().alpha += 0.2f * Time.deltaTime;
        }

        // Clamp down values
        curSustenanceTimer = Mathf.Clamp(curSustenanceTimer, 0, maxSustenanceTimer+1);

        // While the timer is less than the max limit...
        if (curSustenanceTimer <= maxSustenanceTimer && sustenanceCanvas.activeSelf)
        {
            // Countdown
            if(!isHalfTime) //food supply temporarily stops decreasing during half time by lim
            {
            	curSustenanceTimer += (1 / sustenanceTimerSpeed) * Time.deltaTime;
            	sustenanceSpeedTimer += 1 * Time.deltaTime;
            }

            // When the game timer counts to two minutes, begin increasing droughtTimerSpeed
            if (sustenanceSpeedTimer > 120)
                IsTimerSpeedingUp = true;

            if(IsTimerSpeedingUp && sustenanceSpeedTimer > 60)
            {
                // Reduce time and reset game timer
                sustenanceTimerSpeed -= 0.2f;
                sustenanceSpeedTimer = 0;
            }

            // Show it on the UI
            sustenanceBar.fillAmount = (maxSustenanceTimer - curSustenanceTimer) / maxSustenanceTimer;
            sustenanceUIText.text = "Food Supply: " + (int)(maxSustenanceTimer - curSustenanceTimer);
        }
    }

    // Managaes the phases of the game
    void Phases ()
	{
		// Count timers down
		gameTimer += 1 * Time.deltaTime;
		phaseTimer += 1 * Time.deltaTime;

		// Float variable that changes with boolean
		float maxTime = (isHalfTime) ? 30.0f : 360.0f;
        float rainFloat = (isDroughtPhase) ? -0.1f : 0.1f;
        float mistFloat = (isDroughtPhase) ? 0.0f : 0.5f;

        //rain effect is now disabled in the game
        /*
        // Tones the rain down as the phases change
        rain.GetComponent<DigitalRuby.RainMaker.RainScript>().RainIntensity += rainFloat * Time.deltaTime;
        rain.GetComponent<DigitalRuby.RainMaker.RainScript>().RainMistThreshold = mistFloat;

        // Clamps the rain value to not go below 0 or above 1
        rain.GetComponent<DigitalRuby.RainMaker.RainScript>().RainIntensity = Mathf.Clamp(rain.GetComponent<DigitalRuby.RainMaker.RainScript>().RainIntensity, 0, 1);
        */

        // Updates the clock in the UI
        //clockUI.fillAmount = phaseTimer / maxTime;

		// When the phase ends...
		if (phaseTimer >= maxTime) 
		{
			// If it's currently halftime, change the previous phase.
			if (isHalfTime)
			{
				halfTimeNotif.SetActive(false);
			}
			else
			{
				isDroughtPhase = !isDroughtPhase;
				halfTimeNotif.SetActive(true);
			}//swapped by lim

			// Change the phase
			isHalfTime = !isHalfTime;

			// Reset the timer
			phaseTimer = 0;
		}

		// Order changes as game goes on
		if (gameTimer <= 90) 
		{
			minOrderRange = 0;
			maxOrderRange = 0;

			//phaseIncrement2 = 1;
		}

		if (gameTimer > 90 && gameTimer < 210) 
		{
			minOrderRange = 1;
			maxOrderRange = 2;
			//Debug.Log("J Coconuts and B Smoothies should start appearing");

			//phaseIncrement2 = 1;
		}

        if (gameTimer > 210)
        {
			minOrderRange = 2;
			maxOrderRange = 5;
			//Debug.Log("M Smoothies and N Smoothies should start appearing");
           // phaseIncrement1 = 2;
           // phaseIncrement2 = 2;
        }
    }

    // Spawns rice-carrying goats
    void SpawnRiceGoats()
    {
        // Removes null values from the list
        for (int i = 0; i < riceGoatList.Count; i++)
        {
            if (riceGoatList[i] == null)
                riceGoatList.RemoveAt(i);
        }

        // If there's not a lot of goats yet...
        if (riceGoatList.Count < 1)
        {
            // Count a timer down
            riceGoatTimer += 1 * Time.deltaTime;

            // When the timer reaches a certain limit...
            if (riceGoatTimer > 5f)
            {
                // Spawn a goat and add it to the list
                riceGoatList.Add(Instantiate(riceGoatPrefab, riceGoatSpawn.transform.position, Quaternion.identity));

                // Reset timer
                riceGoatTimer = 0;
            }
        }
    }

    // Updates UI + issues new quests
    void OrdersAndSubmissions()
    {
        // If the list of orders isn't full yet and the drought bar is active...
        if(!isListFull() && sustenanceCanvas.activeSelf && !isHalfTime)
        {
            // Start order countdown
            orderTimer += 0.5f * Time.deltaTime;

            // Once a minute has passed, add a new order
            if (orderTimer > 11.0f)
            {
                MakeAnOrder();
                orderTimer = 0;
            }
        }
    }

    // Returns false if the list has a null
    bool isListFull()
    {
        for (int i = 0; i < orderList.Count; i++)
        {
            if (orderList[i] == null)
                return false;
        }
        return true;
    }

    // Scans the list for null values. If there is, replace it with a new order
    void MakeAnOrder()
    {
        for (int i = 0; i < orderList.Count; i++)
        {
            // If a null is found, replace it and break the loop
            if (orderList[i] == null)
            {
                // Set a random number to randomise the order, depending on the phase
                //int rand = (isDroughtPhase) ? Random.Range(1 + phaseIncrement1, 1 + phaseIncrement2) : Random.Range(6, 10);

				int rand = (isDroughtPhase) ? Random.Range(1 + minOrderRange, maxOrderRange) : Random.Range(6, 10);

                // Create the order
                GameObject newOrder = Instantiate(orderPrefab, orderNoticeboard.transform);
                newOrder.GetComponent<Orders>().CreateNewOrder(CreateList(rand),AssignOrderName(rand),AssignTimerLimit(rand), AssignTokenReward(rand), AssignSustenanceIncrement(rand), AssignScoreRewardAmt(rand));
                orderList[i] = newOrder.GetComponent<Orders>();
               
				if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 15)
				{
					tutMgr.GetComponent<NewTutorialMgrScript>().submitBtn = newOrder;
					tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(16);
				}
                	


            	
                //GameObject newGoat = Instantiate(goatPrefab, goatSpawn.transform.position, Quaternion.identity);
                //goatManager.goats.Add(newGoat.GetComponent<PackGoatScript>());

				sfxManager.GetComponent<SfxMgrScript>().PlaySfx(3);
                break;
            }
        }
    }

    // Assigns a name given an int
    string AssignOrderName(int i)
    {
        switch (i)
        {
            case 1:
                return "Coconut Juice";

            case 2:
                return "Jumbo Coconut Juice";

            case 3:
                return "Banana Smoothie";

            case 4:
                return "Mango Smoothie";

            case 6:
                return "Jumbo Coconut";

            case 7:
                return "Banana Meal";

            case 8:
                return "Mango Meal";

            case 9:
                return "Jumbo Meal";

            case 10:
                return "Fruity Meal";

            default:
                return "Nutri-Smoothie";
        }
    }

    // Assigns a time given an int
    int AssignTimerLimit(int i)
    {
        switch (i)
        {
            case 1:
                return 90;

            case 2:
                return 90;

            case 3:
                return 75;

            case 4:
                return 75;

            default:
                return 45;
        }
    }

    // Creates a list of items for an order
    List<GameObject> CreateList(int i)
    {
        List<GameObject> tempList = new List<GameObject>();

        switch(i)
        {
            case 1:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                break;

            case 2:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                break;

            case 3:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                break;

            case 4:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                break;

            case 6:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                break;

            case 7:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                break;

            case 8:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                break;

            case 9:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                break;

            case 10:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                break;

            default:
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[0]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[1]);
                tempList.Add(itemDB.itemDatabase[3]);
                tempList.Add(itemDB.itemDatabase[3]);
                break;
        }

        return tempList;
    }

	int AssignTokenReward(int i)
    {
        switch (i)
        {
            case 1:
                return 1;

            case 2:
                return 1;

            case 3:
                return 2;

            case 4:
                return 2;

            case 6:
                return 1;

            case 7:
                return 1;

            case 8:
                return 1;

            case 9:
                return 1;

            case 10:
                return 1;

            default:
                return 3;
        }
    }

	int AssignSustenanceIncrement(int i)
    {
        switch (i)
        {
            case 1:
                return 12;

            case 2:
                return 15;

            case 3:
                return 20;

            case 4:
                return 20;

            case 6:
                return 25;

            case 7:
                return 28;

            case 8:
                return 28;

            case 9:
                return 28;

            case 10:
                return 28;

            default:
                return 32;
        }
    }

	int AssignScoreRewardAmt(int i)
    {
        switch (i)
        {
            case 1:
                return 200;

            case 2:
                return 350;

            case 3:
                return 500;

            case 4:
                return 500;

            case 6:
                return 1250;

            case 7:
                return 1500;

            case 8:
                return 1500;

            case 9:
                return 1500;

            case 10:
                return 2000;

            default:
                return 1000;
        }
    }

    // Reduces the drought bar
    public void ReduceDrought(int amountReduced)
    {
        curSustenanceTimer -= amountReduced;
    }

    // Detects items from player's inventory
    bool DetectItem(List<GameObject> itemCost)
    {
        // Creates a duplicate of the inventory
        List<GameObject> playerInvCopy = new List<GameObject>(invSystem.playerInventory);

        // For every item in itemCost
        for (int i = 0; i < itemCost.Count; i++)
        {
            // If there's a copy of it in the player's inventory, remove it and continue the loop
            if (playerInvCopy.Contains(itemCost[i]))
                playerInvCopy.Remove(itemCost[i]);
            else return false;   // If there is no copy, return false and end the function prematurely
        }
        return true;   // Returns true if all items are there
    }

    // ================================= Button functions =================================

    //Shows the drought UI + hides the button
    public void ShowBar()
    {
        sustenanceCanvas.SetActive(true);
        //clockUICanvas.SetActive(true);
        MakeAnOrder();
    }

    // Complete an order in the list
    public void FinishOrder()
    {
        // For every order in the list
        for (int i = 0; i < orderList.Count; i++)
        {
            // If the player has the items in their inventory for one order...
            if (orderList[i] != null && DetectItem(orderList[i].order))
            {
                // Remove the items from the player's inventory
                for (int j = 0; j < orderList[i].order.Count; j++)
                {
                    for (int k = 0; k < invSystem.playerInventory.Count; k++)
                    {
                        // Compare the COST LIST in orderList[i] with the player's inventory
                        // If the item in the inventory is the same as the one in the list...
                        if (orderList[i].order[j] == invSystem.playerInventory[k])
                        {
                            // Null the item, destroy the corresponding button and re-loop
                            invSystem.playerInventory[k] = null;
                            Destroy(invSystem.slots[k].GetComponentInChildren<ItemButton>().gameObject);
                            break;
                        }
                    }
                }

                // Delete the order, reduce drought, award tokens, send pack goat on its way
				
                Destroy(orderList[i].gameObject);

                orderList[i] = null;
                //ReduceDrought(20);
                //invSystem.tokens += 3;
                //goatManager.SendGoatOff();

                // Stop the function entirely
                return;
            }
        }
        Debug.Log("You do not have the required items!");
    }

    public void Cheat() //added by lim, resets the sustenance bar & sets energy to 100
    {
    	curSustenanceTimer = 0;
    	player.GetComponent<SkillsScript>().curEnergy = 100;
    }
}
