﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiggingScript : MonoBehaviour
{
    // Digging, shopping and planting values
    public float digSpeed; //added by lim for perk
    public float digTimer = 0.0f;
    public GameObject soilPrefab;
    public GameObject nearbySoil = null;
    public GameObject shovel1, shovel2;
    public bool isDigging = false;

    // Attatched Player UI
    public GameObject diggingCanvas;
    public Image digBar;

    // Script refs
    NewPlayerScript playerScript;
    SkillsScript skillsScript;

    public GameObject tutMgr; // Tutorial Manager by Lim
    public GameObject flashMgr;

    // Start is called before the first frame update
    private void Start()
    {
        flashMgr = GameObject.Find("UIFlashMgr");
        tutMgr = GameObject.Find("Tutorial Manager");
        playerScript = this.GetComponent<NewPlayerScript>();
        skillsScript = this.GetComponent<SkillsScript>();
    }

    private void Update()
    {
        Dig2();
        ShovelUI();
    }

    // Updates shovel-based UI elements
    private void ShovelUI()
    {
        shovel1.SetActive(!shovel2.activeSelf);

        // Update the dig timer UI
        diggingCanvas.SetActive(digTimer > 0);
        digBar.fillAmount = digTimer / 1;
    }

    // private void Dig()
    // {
    //     // Removes control of the player if the RMB is held, and removes current velocity
    //     playerScript.isInControl = !Input.GetMouseButton(2);
    //     if (Input.GetMouseButton(2))
    //     {
    //         playerScript.rb.velocity = Vector3.zero;
    //         playerScript.moveLoc = playerScript.transform.position;
    //     }

    //     // If the right mouse button is clicked...
    //     switch (Input.GetMouseButton(1))
    //     {
    //         case true:
    //             // If the shovel is still useable...
    //             if (skillsScript.CheckEnergy(10) && !playerScript.nearSoil)
    //             {
    //                 // Count a timer down
    //                 playerScript.isInControl = false;
    //                 playerScript.unitAnim.SetBool("isDigging", true);
    //                 digTimer += digSpeed * Time.deltaTime;
    //                 shovel2.SetActive(true);

    //                 // If the timer reaches 2, and the player is not near a soil mound...
    //                 if (digTimer >= 1 && !playerScript.nearSoil)
    //                 {
    //                     // Cast a ray, originating from the player and pointing downwards
    //                     Ray rayTwo = new Ray();
    //                     rayTwo.origin = transform.position;
    //                     rayTwo.direction = -transform.up;
    //                     RaycastHit hitTwo;

    //                     // If the ray hits the soil, instantiate the mound there
    //                     if (Physics.Raycast(rayTwo, out hitTwo, 100.0f))
    //				if (hitTwo.collider.tag == "Soil")
    //                         	{
    //                             	GameObject soil = Instantiate(soilPrefab, hitTwo.point, Quaternion.identity);

    //						if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 2) //Inserted planting tutorial here
    //						{
    //                         			tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(2);
    //							tutMgr.GetComponent<NewTutorialMgrScript>().firstDigSpot = soil;
    //                         		}
    //                         	}

    //                     // Reduce shovel life and durability
    //                     skillsScript.curEnergy -= 10;

    //                     // Reset the timer
    //                     digTimer = 0;

    //                     // Set hasDug to true for tutorial stuff
    //                     playerScript.hasDug = true;
    //                 }
    //             }

    //else if (skillsScript.CheckEnergy(10) == false)
    //	flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashYellow(0));

    //             break;

    //         // Decrease dig timer until it is zero
    //         default:
    //             digTimer = 0;
    //             shovel2.SetActive(false);
    //             playerScript.unitAnim.SetBool("isDigging", false);
    //             break;
    //     }
    // }

    private void Dig2()
    {
        // If the RMB is clicked once -AND- the player has enough energy to dig -AND- is not near a mound -AND is not doing any digging action...
        if (Input.GetMouseButton(1) && (skillsScript.CheckEnergy(10) && !playerScript.nearSoil) && !isDigging && !playerScript.nearWater)
        {
            isDigging = true;
            playerScript.isInControl = false;
			GetComponent<NewPlayerScript>().moveLoc = this.transform.position;
            //StartCoroutine(DigCountdown(digTimer, 1 / digSpeed));
        }
        else if (!isDigging)
        {
            digTimer = 0;
            shovel2.SetActive(false);
            playerScript.unitAnim.SetBool("isDigging", false);

            if (!skillsScript.CheckEnergy(10))
                flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashYellow(0));
        }

        // Starts digging
        if (isDigging)
        {
            playerScript.unitAnim.SetBool("isDigging", true);
            shovel2.SetActive(true);
            digTimer += digSpeed * Time.deltaTime;

            if (digTimer >= 1)
            {
                // Cast a ray, originating from the player and pointing downwards
                Ray rayTwo = new Ray();
                rayTwo.origin = transform.position;
                rayTwo.direction = -transform.up;
                RaycastHit hitTwo;

                // If the ray hits the soil, instantiate the mound there
                if (Physics.Raycast(rayTwo, out hitTwo, 100.0f))
                    if (hitTwo.collider.tag == "Soil")
                    {
                        GameObject soil = Instantiate(soilPrefab, hitTwo.point, Quaternion.identity);

                        if (tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 2) //Inserted planting tutorial here
                        {
                            tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(2);
                            tutMgr.GetComponent<NewTutorialMgrScript>().firstDigSpot = soil;
                        }

						if (tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 11) //Inserted planting tutorial here
                        {
                            tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(11);
                            tutMgr.GetComponent<NewTutorialMgrScript>().secondDigSpot = soil;
                            soil.GetComponent<MainTreeScript>().secondTree = true;
                        }
                    }

                // Reduce shovel life and durability
                skillsScript.curEnergy -= 10;

                // Reset the timer
                digTimer = 0;

                // Set hasDug to true for tutorial stuff
                playerScript.hasDug = true;
                playerScript.isInControl = true;
                isDigging = false;
            }
        }
    }
}