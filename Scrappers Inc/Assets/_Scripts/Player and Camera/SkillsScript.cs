﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillsScript : MonoBehaviour
{
    // Energy values
    public float curEnergy;
    float maxEnergy = 100.0f;

    // Current "skill" to be used, based on item system
    public GameObject curSkill;
    int skillID;

    // UI Elements
    public Image energyBar;
    public GameObject[] seedIconHighlight;

    public GameObject tutMgr;
    public GameObject flashMgr;

    // Misc references
    ItemDatabase itemDB;
    NewPlayerScript playerScript;

    //Info stuff
    public GameObject infoPanel;
	public Text skillTitle;
	public Text skillDesc;
	public Text energyCost;

	public string[] skillTitleString;
	public string[] skillDescString;
	public string[] energyCostString;

	public Button wBtn;
	public Button eBtn;

	public Image wBtnImg;
	public Image eBtnImg;

	public Image[] skillImgs;

	public Text skillLabel;

    // Start is called before the first frame update
    void Start()
    {
		tutMgr = GameObject.Find("Tutorial Manager");
		flashMgr = GameObject.Find("UIFlashMgr");
		tutMgr = GameObject.Find("Tutorial Manager");
        playerScript = GetComponent<NewPlayerScript>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();

        // Set default skill to coconut
        skillID = 0;
        curSkill = itemDB.itemDatabase[skillID];
		TransparentSkillUI(true);
    }

    // Update is called once per frame
    void Update()
    {
        // Clamps energy value
        if (curEnergy > maxEnergy) curEnergy = maxEnergy;
        else if (curEnergy < 0) curEnergy = 0;

        KeyCommands();
        EnergyUI();
    }

    // Checks if the player's remaining energy is equal or less than the int
    public bool CheckEnergy(float minVal)
    {
        if (minVal <= curEnergy) return true;
        else return false;
    }

    // Energy UI Manager
    void EnergyUI()
    {
        energyBar.fillAmount = curEnergy / maxEnergy;

        for (int i = 0; i < seedIconHighlight.Length; i++)
        {
            seedIconHighlight[i].SetActive(i == skillID / 2);
        }
    }

    // Plants a seed if the player is nearby and reduce energy by this amount
    public void PlantSeed(float reduceEnergy, int seedIndex)
    {
        // If the player is near unplanted soil, and has sufficient energy...
        if (playerScript.nearbySoil && !playerScript.nearbySoil.GetComponent<MainTreeScript>().isPlanted && CheckEnergy(reduceEnergy))
        {
			curSkill = itemDB.itemDatabase[seedIndex];

            playerScript.nearbySoil.GetComponent<MainTreeScript>().PlantTree(curSkill.GetComponent<Seed>());
            curEnergy -= reduceEnergy;
			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 3)
			{
                tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(3);
            }
			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 12)
			{
                tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(12);
            }
        }

		else if(CheckEnergy(reduceEnergy) == false)
			flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashYellow(0));

    }

    // Keyboard-based commands for the player. Consumes energy
    void KeyCommands()
    {
        //Plants seeds for QWE
        if (Input.GetKeyDown(KeyCode.Q))
        {
        	
            skillID = 0;
            curSkill = itemDB.itemDatabase[skillID];

            PlantSeed(10,0);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            skillID = 4; //skill ID used to be 2, swapped by lim with the E skill button to plant the correct tree
            curSkill = itemDB.itemDatabase[skillID];

            PlantSeed(30,4);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            skillID = 2;
            curSkill = itemDB.itemDatabase[skillID];

            PlantSeed(30,2);
        }

        //Fertilises for R
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (playerScript.nearbySoil && !playerScript.nearbySoil.GetComponent<TreeFruitScript>().isFertilised && playerScript.nearbySoil.GetComponent<MainTreeScript>().stage >= 4)
            {

				if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 15)
				{
                    tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(15);	
					wBtnImg.GetComponent<Image>().color = new Color32 (255,255,255,255);
					eBtnImg.GetComponent<Image>().color = new Color32 (255,255,255,255);
                }

				if(CheckEnergy(10))
				{
                	itemDB.itemDatabase[6].GetComponent<Powerup>().ActivatePowerUP(itemDB.itemDatabase[6].GetComponent<Powerup>().powerUpType);
                	curEnergy -= 10;
	
                }
                else
					flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashYellow(0));
            }
        }
    }

    // Button function to set the seed type
    public void SetSeed(int skillSet)
    {
        skillID = skillSet;

        //PlantSeed
        if(skillSet == 0)
			PlantSeed(10, skillSet);
		else
			PlantSeed(30, skillSet);

        //PlantSeed((skillSet == 0) ? 10 : 30);
    }

    // Button function to fertilise nearby soil
    public void Fertilise()
    {
        if (playerScript.nearbySoil && !playerScript.nearbySoil.GetComponent<TreeFruitScript>().isFertilised && playerScript.nearbySoil.GetComponent<MainTreeScript>().stage >= 4)
        {
			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 15)
			{
				tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(15);
				wBtnImg.GetComponent<Image>().color = new Color32 (255,255,255,255);
				eBtnImg.GetComponent<Image>().color = new Color32 (255,255,255,255);
			}
			if(CheckEnergy(10))
			{
            	itemDB.itemDatabase[6].GetComponent<Powerup>().ActivatePowerUP(itemDB.itemDatabase[6].GetComponent<Powerup>().powerUpType);
            	curEnergy -= 10;
				   		
            }
            else
				flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashYellow(0));
        }
    }

	public void ShowSkillInfo(int infoIndex)
    {
		infoPanel.SetActive(true);

		skillTitle.text = skillTitleString[infoIndex];
		skillDesc.text = skillDescString[infoIndex];
		energyCost.text = energyCostString[infoIndex];

	}

	public void HideSkillInfo()
	{
		infoPanel.SetActive(false);
	}

	public void TransparentSkillUI(bool makeTransparent)
	{
		if(makeTransparent == true)
		{
			for(int i = 0; i < skillImgs.Length; i++)
			{
				skillImgs[i].color = new Color32 (255,255,255,100);
			}
			skillLabel.color = new Color32 (200,250,125,150);
		}
		else
		{
			for(int i = 0; i < skillImgs.Length; i++)
			{
				skillImgs[i].color = new Color32 (255,255,255,255);
			}
			skillLabel.color = new Color32 (200,250,125,255);
		}
	}

}
