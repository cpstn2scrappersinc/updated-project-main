﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WateringScript : MonoBehaviour
{
    // Script refs
    NewPlayerScript playerScript;

    // Water Values
    public float maxWaterLvl;
    public float waterRechargeTimer = 0.0f;
    public float curWaterLevel = 0.0f;
    public GameObject WaterSystem;
    public GameObject WaterCollider;
    public bool refilling = false, isWatering = false;
    public GameObject waterTarget;

    // UI values
    public Image waterBar, gatherBar;
    public Text waterPercentText;
    public GameObject wateringCanvas, wateringPrompt;
    public GameObject bucket;

    // Perks
    public bool waterPerkUnlocked = false;

    // Misc
    public GameObject tutMgr;
	public GameObject flashMgr;


    private void Start()
    {
		flashMgr = GameObject.Find("UIFlashMgr");
		tutMgr = GameObject.Find("Tutorial Manager");
        playerScript = this.GetComponent<NewPlayerScript>();
        waterTarget = null;

        maxWaterLvl = 100;
    }

    private void Update()
    {
        WaterUI();
        FillWater();

        if(waterTarget)
            Watering(waterTarget);
    }

    // Fills up the bucket with water
    public void FillWater()
    {
        if (refilling)
        {
            // Start filling the bucket with water
            waterRechargeTimer += 0.5f * Time.deltaTime;
            playerScript.isInControl = false;

            // When the timer passes through a certain point, set levels to max
            if (waterRechargeTimer >= 1.5f)
            {
                curWaterLevel = maxWaterLvl;
                waterRechargeTimer = 0;
                playerScript.isInControl = true;

                if (tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 5)
                    tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(5);

                flashMgr.GetComponent<UiFlashMgrScript>().StartCoroutine(flashMgr.GetComponent<UiFlashMgrScript>().FlashGreen(1));

                refilling = false;
            }
        }
    }

    // Waters the objects in front of the player
    public void Watering(GameObject target)
    {
        if (isWatering)
        {
            Debug.Log("Working!");
            //Prevent the water can from reaching negative values
            if (curWaterLevel < 0)
                curWaterLevel = 0;

            // Activate water GameObjects
            WaterSystem.SetActive(true);
            WaterCollider.SetActive(true);
            bucket.SetActive(true);
            playerScript.unitAnim.SetBool("isWatering", true);
            playerScript.isInControl = false;
            GetComponent<NewPlayerScript>().moveLoc = this.transform.position;

            // Turn the player around to face the target
            Vector3 turn = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
            transform.LookAt(turn);

            // Water the target, then cancel if it is fully watered.
            if (!target.GetComponent<TreeWaterScript>().isWatered)
            {
                curWaterLevel -= 10 * Time.deltaTime;
                target.GetComponent<TreeWaterScript>().curWaterLvl += 1 * Time.deltaTime;
            }
            else if(target.GetComponent<TreeWaterScript>().isWatered)
            {
                // Deactivate values
                WaterSystem.SetActive(false);
                WaterCollider.SetActive(false);
                bucket.SetActive(false);
                playerScript.unitAnim.SetBool("isWatering", false);
                playerScript.isInControl = true;
                waterTarget = null;
                isWatering = false;
                if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 7)
                {
					tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(7);
                }
            }

        }
    }
    
    // Updates the water UI elements, based on the object in hand
    private void WaterUI()
    {
        // Set maxWaterLvl depending on perk
        maxWaterLvl = (waterPerkUnlocked) ? 300 : 100;

        // Fills the bar based on current water levels
        waterBar.fillAmount = curWaterLevel / maxWaterLvl;

        // Update the water gathering UI
        wateringCanvas.SetActive(waterRechargeTimer > 0);
        gatherBar.fillAmount = waterRechargeTimer / 1.5f;

        // [LIM]
        // Water prompt for when you're near a mound
        if (playerScript.gameCtrlr.GetComponent<GameControllerScript>().gameTimer == 0)
				wateringPrompt.SetActive(playerScript.nearbySoil && playerScript.nearbySoil.GetComponent<MainTreeScript>().stage <= 1 && !playerScript.nearbySoil.GetComponent<TreeWaterScript>().isWatered && playerScript.nearbySoil.GetComponent<MainTreeScript>().isPlanted);
    }
}
