﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //    // Set Target object for camera to follow in inspector
    //    public Transform target;

    //    // Distance variables, modify in inspector
    //    // Initial distance
    //    public float distance = 5.0f;
    //    float savedDistance = 5.0f;

    //    // Mouse turn speed
    //    public float xSpeed = 120.0f;
    //    public float ySpeed = 120.0f;

    //    public float yMinLimit = -20f;
    //    public float yMaxLimit = 80f;

    //    public float distanceMin = .5f;
    //    public float distanceMax = 15f;

    //    Vector3 vecRef = Vector3.zero;

    //    float x = 0.0f;
    //    float y = 0.0f;

    //    // Use this for initialization
    //    void Start()
    //    {
    //        // Initial values
    //        Vector3 angles = transform.eulerAngles;
    //        x = angles.y;
    //        y = angles.x;

    //        // Make the rigid body not change rotation
    //        if (GetComponent<Rigidbody>() != null)
    //            GetComponent<Rigidbody>().freezeRotation = true;
    //    }

    //    void LateUpdate()
    //    {
    //        // If there is a target set in inspector...
    //        if (target)
    //        {
    //            // Input for rotating, based on mouse movement
    //            x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
    //            y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

    //            // Target rotation

    //            // Clamp Y-axis (up-down) movement to prevent it from rotating wildly up and down
    //            y = ClampAngle(y, yMinLimit, yMaxLimit);

    //            // Rotation value based on mouse inputs
    //            Quaternion rotation = Quaternion.Euler(y, x, 0);
    //            Quaternion targetRot = Quaternion.Euler(0, x, 0);

    //            // Set the camera's distance based on mouse wheel input
    //            savedDistance = Mathf.Clamp(savedDistance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
    //            distance = savedDistance;

    //            // Raycast for camera collisions. If there's a hit, zoom camera in to prevent hit object from blocking view
    //            RaycastHit hit;
    //            if (Physics.Linecast(target.position, transform.position, out hit))
    //            {
    //                if (hit.collider.tag != "TutorialCollider")
    //                    distance = hit.distance;
    //            }
    //            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
    //            Vector3 position = rotation * negDistance + target.position;

    //            // Set position and rotation based on inputs
    //            transform.rotation = rotation;
    //            target.transform.rotation = targetRot;
    //            transform.position = Vector3.SmoothDamp(this.transform.position, position, ref vecRef, 0.15f);
    //        }
    //    }

    //    // Clamp function
    //    public static float ClampAngle(float angle, float min, float max)
    //    {
    //        if (angle < -360F)
    //            angle += 360F;
    //        if (angle > 360F)
    //            angle -= 360F;
    //        return Mathf.Clamp(angle, min, max);
    //    }
    //}

    /*
     * // ===OLD CODE 2===

    public GameObject target;
    //public GameObject targetCamPos;
    public float rotateSpeed = 5;
    GameController gamCon;

    Vector3 offset;

    void Start()
    {
        SetTarget(GameObject.FindWithTag("Player"));
        offset = target.transform.position - transform.position;
        //gamCon = GameObject.FindGameObjectWithTag("GameController").GetComponentInParent<GameController>();
    }

    // Moves the camera based on mouse position and rotates the player as well
    void LateUpdate()
    {
        MoveCam();
    }

    void MoveCam()
    {
        float horizontal = Input.GetAxisRaw("Mouse X") * rotateSpeed;
        //float vertical = Input.GetAxisRaw("Mouse Y") * rotateSpeed;

        target.transform.Rotate(0, horizontal, 0);

        float desiredAngle = target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);

        transform.position = target.transform.position - (rotation * offset);

        //if (target.GetComponent<PlayableUnit>().isMounted)
        //    transform.position = new Vector3(transform.position.x, 6.8f, transform.position.z);

        transform.LookAt(target.transform);
    }

    // Sets the new target of the camera, to be used by CharMgrScript
    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;
        //targetCamPos = target.GetComponent<PlayableUnit>().camPos;
        //this.transform.SetParent(targetCamPos.transform);
        //this.transform.localPosition = new Vector3(0,0,0);
    }
    */


    // ===OLD CODE 1===
    /*
    */

    public GameObject target;
    public float damping = 1;
    Vector3 offset;

    void Start()
    {
        offset = transform.position - target.transform.position;
    }

    void LateUpdate()
    {
        Vector3 desiredPosition = target.transform.position + offset;
        Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        transform.position = position;

        //transform.LookAt(target.transform.position);
    }
}