﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerksScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject scoreMgr;
	public GameObject perkInfo;
	public GameObject perkNotifCanvas;

    public bool fruitPerkUnlocked;
    //public bool perkInfoActive;

    //public GameObject[] perkButtons;

    //public string perkR
    public GameObject perkUI;
	public GameObject[] perkUiObjs;
    public Image[] perkImages;
    public Button[] perkButtons;
    public string[] perkTitleString;
	public string[] perkDescString;

    public Text perkTitle;
	public Text perkDesc;
	public Text perkReq;

	public Text perkLabel;

    public float requiredScore;

    public bool[] isPerkIndexUnlocked;

    void Start()
    {
        scoreMgr = GameObject.Find("ScoreManager");
        PerkInfoTransparent(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UnlockPerk(int perkIndex)
    {
		if(scoreMgr.GetComponent<ScoreMgrScript>().curScore >= requiredScore)
		{
    		if(perkIndex == 0) // unlock giant water can
    		{
				//changes the cur and max water level variables in the watering script
				GetComponent<WateringScript>().maxWaterLvl = 300;
				GetComponent<WateringScript>().curWaterLevel = 300;
				GetComponent<WateringScript>().waterPerkUnlocked = true;

    		}
			else if (perkIndex == 1) // unlock fruity fuel
    		{
				//Fruit in the ItemButtonScript now checks this condition for whether or not fruits should give extra energy
				fruitPerkUnlocked = true;
    		}

			else if (perkIndex == 2) //Unlock speedy toiler
    		{
				//changes the walk and run speed in newplayerscript, changes digspeed in Digging Script
				GetComponent<NewPlayerScript>().walkSpeed = 12;
				GetComponent<NewPlayerScript>().runSpeed = 12;
				GetComponent<DiggingScript>().digSpeed = 0.5f;
    		}

			HidePerkInfo();
			//DisableButton(perkIndex);
			isPerkIndexUnlocked[perkIndex] = true;
			perkButtons[perkIndex].interactable = !perkButtons[perkIndex].interactable;
			requiredScore *= 2.5f;
			PerkInfoTransparent(true);
			perkNotifCanvas.SetActive(false);

			//refreshes displayed info

			//ShowPerkInfo(perkIndex);
    	}

    }

    public void ShowPerkInfo(int infoIndex)
    {
    	perkInfo.SetActive(true);

		perkTitle.text = perkTitleString[infoIndex];

    	if(isPerkIndexUnlocked[infoIndex] == false)
			perkReq.text = "Score to Unlock: " + requiredScore;

    	else
			perkReq.text = "Unlocked";

    	perkDesc.text = perkDescString[infoIndex];

    }

    public void PerkInfoTransparent(bool isTransparent)
    {
    	if(isTransparent)
    	{	
    		for(int i = 0; i < perkUiObjs.Length; i++)
    		{
				perkUiObjs[i].GetComponent<Image>().color = new Color32 (255,255,255,100);
    		}
			perkLabel.color = new Color32 (125,240,245,100);
    	}
    	else
    	{
			for(int i = 0; i < perkUiObjs.Length; i++)
    		{
				perkUiObjs[i].GetComponent<Image>().color = new Color32 (255,255,255,255);
    		}
			perkLabel.color = new Color32 (125,240,245,255);
    	}
    }

	public void HidePerkInfo()
    {
		perkReq.text = "";
		perkInfo.SetActive(false);
    }

    public void DisableButton(int imageIndex)
    {
		perkButtons[imageIndex].interactable = !perkButtons[imageIndex].interactable;
    	perkImages[imageIndex].GetComponent<Image>().color = new Color32 (255,255,255,150);
    }
}
