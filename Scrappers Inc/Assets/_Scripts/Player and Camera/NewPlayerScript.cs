﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NewPlayerScript : MonoBehaviour
{
    // The unit's rigidbody
    public Rigidbody rb;

    // Unit speed variables
    public float runSpeed;
    public float walkSpeed;
    public float jumpMagnitude = 4.0f;
    float curSpeed;
    public Vector3 moveLoc;

    // Prefabs, GameObject holders for detection and boolean for player action
    public GameObject soilPrefab;
    public GameObject nearbySoil = null;
    public GameObject nearbyGoat = null;
    public GameObject nearbyShop = null;

    // Attatched Player UI
    public GameObject playerUI;

    // Misc values
    public WateringScript waterScript;
    public InventorySystem invSystem;
    public ItemDatabase itemDB;
    public SkillsScript skillsScript;
    public bool isInControl;
    public bool nearSoil;
    public bool nearWater;
    public Animator unitAnim;
    Camera cam;

    // Boolean values for tutorial stuff
    public bool hasMoved = false;
    public bool hasDug = false;

	public GameObject sfxManager; //added by lim
	public GameObject gameCtrlr;
	public GameObject tipsMgr;

	//public GameObject skillCanvas;
	public GameObject tutMgr;



	//public

    // Use FixedUpdate for physics/movements
    void FixedUpdate()
    {
        if(isInControl)
            Move();
    }

    private void Update()
    {
        // Other actions
        ClickCommand();
        PlayerUIUpdate();
    }

    void Start()
    {
        // Set initial variables
		tutMgr = GameObject.Find("Tutorial Manager");
        unitAnim = GetComponentInChildren<Animator>();
        invSystem = this.gameObject.GetComponent<InventorySystem>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        waterScript = GetComponent<WateringScript>();
        skillsScript = GetComponent<SkillsScript>();
        cam = Camera.main;

        moveLoc = this.transform.position;
    }

    // Updates UI Elements
    void PlayerUIUpdate()
    {
        // Billboard effect
        playerUI.transform.LookAt(playerUI.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);
    }

    // Shows whether or not the movement keys are being pressed
    bool IsMoving()
    {
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            hasMoved = true;
            return true;
        }
        else return false;
    }

    // Movement script
    void Move()
    {
        // Running faster by pressing shift
        bool running = Input.GetKey(KeyCode.LeftShift);
        curSpeed = (running) ? runSpeed : walkSpeed;

        // Get the vector3 coordinates from the moveLoc variable, set by right clicking on the ground
        Vector3 movement = new Vector3(moveLoc.x, this.transform.position.y, moveLoc.z);

        // Turn the character to look at the location
        transform.LookAt(movement);

        if (Vector3.Distance(movement, this.transform.position) > 0.75f)
        {
            //Debug.Log(Vector3.Distance(movement, this.transform.position));
            transform.position += transform.forward * curSpeed * Time.deltaTime;
            unitAnim.SetBool("isMoving", true);
        }
        else
        {
            rb.velocity = Vector3.zero;
            unitAnim.SetBool("isMoving", false);
        }
    }

    // Updates move animations
    void UpdateMoveAnim(Vector3 dir)
    {
        // If there are vectors, set true
        if (dir.x == 0 && dir.z == 0)
            unitAnim.SetBool("isMoving", false);
        else
            unitAnim.SetBool("isMoving", true);

        // Set values in the anim controller
        unitAnim.SetFloat("velocityX", dir.x);
        unitAnim.SetFloat("velocityZ", dir.z);
    }

    // Mouse commands for the player
    private void ClickCommand ()
    {
        // Cast a ray, originating from the camera
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Layermasks
        LayerMask terrain = LayerMask.GetMask("Floor");

        // If the left mouse button is clicked...
        if (Input.GetMouseButtonDown(0))
        {
            // If it hits ...
			if(!EventSystem.current.IsPointerOverGameObject()) // Prevents player from walking when clicking any ui element (eg buttons)
            {
            	if (Physics.Raycast(ray, out hit, 100.0f))
            	{
                    GameObject itemHit = hit.collider.gameObject;

                    // ...an Item, add it to the inventory
                    if (itemHit.GetComponent<Item>() && itemHit.GetComponent<Item>().playerClose)
                	{
                 	   itemHit.GetComponent<Item>().AddItem();   // Add it to the inventory
                 	   sfxManager.GetComponent<SfxMgrScript>().PlaySfx(1);
                	}
                    
                    // ...a fully grown tree, then collect the fruits from it if it has one + inventory is not full
                    // Insert fruit gathering tutorial here
					if (itemHit.GetComponent<MainTreeScript>() && itemHit.GetComponent<MainTreeScript>().stage >= 4 && itemHit == nearbySoil)
                	{
                    // If there's a fruit, collect it
                    	if (itemHit.GetComponent<TreeFruitScript>().curFruits > 0)
                    	{
							nearbySoil.GetComponent<TreeFruitScript>().AddFruit();
                        	sfxManager.GetComponent<SfxMgrScript>().PlaySfx(0);

                        /*if (tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 5) //Inserted order submitting tutorial here
                        {
                            //tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorialPic(5);
                            tutMgr.GetComponent<NewTutorialMgrScript>().StartCoroutine("HideTutorialPic"); // Ends the tutorial
                        }*/
                    	}
                	}

                    if (itemHit.GetComponent<MainTreeScript>() && itemHit.GetComponent<MainTreeScript>().isPlanted && !itemHit.GetComponent<TreeWaterScript>().isWatered && isInControl && waterScript.curWaterLevel > 0 && itemHit == nearbySoil)
                    {
                        waterScript.waterTarget = itemHit;
                        waterScript.isWatering = true;
                    }

                    // ...a well, fill up the bucket with water
                        if (itemHit.tag == "Well" && nearWater && isInControl)
                	{
                        if (waterScript.curWaterLevel < waterScript.maxWaterLvl)  // Prevents player from gathering water with full bucket
                            waterScript.refilling = true;
					}

                	if(itemHit.tag == "Soil" && isInControl)
						moveLoc = hit.point; // player left click movement
                }
            }
        }
    }

    //public voi
}
