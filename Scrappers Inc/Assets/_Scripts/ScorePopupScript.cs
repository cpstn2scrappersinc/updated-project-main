﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePopupScript : MonoBehaviour
{
    // Start is called before the first frame update


    float yInc;
    public int addedScoreAmt;
    public GameObject player;
    public Text popupText;
    public Vector3 textPos;
    public Text textColor;

    public float transparency = 1;

    void Start()
    {
    	player = GameObject.Find("Player");
		popupText.text = "+" + addedScoreAmt + " score ";
        StartCoroutine("DestroyText");
    }

    // Update is called once per frame
    void Update()
    {
		textPos = new Vector3(player.transform.position.x, player.transform.position.y + yInc, player.transform.position.z);

		yInc += 4 * Time.deltaTime;
		transparency -= 0.6f * Time.deltaTime;

		transform.position = Camera.main.WorldToScreenPoint(textPos);
		textColor.GetComponent<Text>().color = new Color(0.29f,1,0.5f,transparency);
    }

    IEnumerator DestroyText()
    {
    	yield return new WaitForSeconds(2);
    	Destroy(gameObject);
    }
}
