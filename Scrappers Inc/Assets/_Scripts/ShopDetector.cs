﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopDetector : MonoBehaviour
{

    // Determines whether or not the player is in the collider
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearbyShop = this.transform.parent.gameObject;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearbyShop = this.transform.parent.gameObject;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearbyShop = null;
    }
}
