﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoilDetector : MonoBehaviour {

    // Determines whether or not the player is in the collider
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearSoil = true;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearSoil = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<NewPlayerScript>())
            other.gameObject.GetComponent<NewPlayerScript>().nearSoil = false;
    }
}
