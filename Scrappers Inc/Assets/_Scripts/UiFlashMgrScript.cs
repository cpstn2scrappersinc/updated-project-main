﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiFlashMgrScript : MonoBehaviour
{
	public GameObject[] greenFlashes; // 0 = energy || 1 = water || 2 = food supply || 3 = inventory
	public GameObject[] yellowFlashes; // 0 = energy || 1 = water

	public bool isFlashing;

	public float flashTimer;

	public GameObject player;
	public GameObject tutMgr;
	
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
		tutMgr = GameObject.Find("Tutorial Manager");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        FlashWater();
    }

    public IEnumerator FlashGreen(int flashIndex)
    {
    	if(isFlashing == false)
    	{
    		isFlashing = true;
			greenFlashes[flashIndex].SetActive(true);
    		yield return new WaitForSeconds(0.33f);
			greenFlashes[flashIndex].SetActive(false);

			isFlashing = false;
		}
    }

	public IEnumerator FlashYellow(int flashIndex)
    {
    	if(isFlashing == false)
    	{
    		isFlashing = true;
    		for(int i = 0; i < 3; i++)
    		{
				yellowFlashes[flashIndex].SetActive(true);
    			yield return new WaitForSeconds(0.2f);
				yellowFlashes[flashIndex].SetActive(false);
				yield return new WaitForSeconds(0.2f);
			}
			isFlashing = false;
		}
    }

    public void FlashWater()
    {
    	if(player.GetComponent<WateringScript>().curWaterLevel <= 0 && tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex > 3)
    	{
    		flashTimer += Time.deltaTime;
    		if(flashTimer >= 1)
    		{
    			if(flashTimer >= 1.5f)
					flashTimer = 0;

    			yellowFlashes[1].SetActive(true);

    		}
    		else if (flashTimer <= 0.1f)
    		{
				yellowFlashes[1].SetActive(false);
    		}
    	}
    	else
			yellowFlashes[1].SetActive(false);
    }
}
