﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxMgrScript : MonoBehaviour 
{
	public List<AudioSource> sfxList;
	public bool isSfxPlaying;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void PlaySfx (int sfxIndex)
	{
		sfxList[sfxIndex].Play();
	}

}

/*
	Sfx List:
	0 = Gathering Sfx
	1 = Item Pickup Sfx
	2 = Goat Feeding Sfx
	3 = New Order Sfx

	4 = Stage 1 Sfx
	5 = Stage 2 Sfx
	6 = Stage 3 Sfx
	7 = Stage 4 / Mature Sfx

	8 = Order Submitted Sfx
	9 = Order Expired Sfx
*/