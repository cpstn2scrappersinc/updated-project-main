﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroCutsceneScript : MonoBehaviour 
{

	public List<GameObject> introPics;
	public int curPicIndex;
	//public GameObject gameCtrlr;


	public void NextSlide ()
	{
		Debug.Log("hi");
		if (curPicIndex < introPics.Count - 1) 
		{
			curPicIndex += 1;
			if (curPicIndex > 0) 
			{
				introPics [curPicIndex - 1].SetActive (false);
			}

			introPics [curPicIndex].SetActive (true);

		} 
		else 
		{
            /*if (introScene == true)
            {
                GetComponent<NewMainMenu>().PlayGame();
            }*/
			//introPics.SetActive(false);
			SceneManager.LoadScene("Game");
           // gameCtrlr.GetComponent<NewMainMenu>().TogglePause();
            
		}
	}
}
