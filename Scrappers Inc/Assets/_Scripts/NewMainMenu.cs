﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NewMainMenu : MonoBehaviour {

	public bool creditsActive;
	public GameObject creditsCanvas;

    GameControllerScript gamCon;
    InventorySystem invSystem;
    NewPlayerScript player;

    public bool isMainMenu;

    // Use this for initialization
    void Start ()
    {
        gamCon = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();

		if(isMainMenu == false) //added by lim to get rid of a null ref exception in main menu scene
		{
			invSystem = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
			player = GameObject.FindWithTag("Player").GetComponent<NewPlayerScript>();
		}
    }
	
	// Update is called once per frame
	void Update () {

    }

    // Gets the int of the next scene after the current
    int nextSceneInt()
    {
        if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings)
            return SceneManager.GetActiveScene().buildIndex + 1;
        else return 0;
    }


    // ================================================================================= Button Functions =================================================================================

    // Pauses the game, hopefully
    public void TogglePause()
    {
        Time.timeScale = Mathf.Approximately(Time.timeScale, 0.0f) ? 1.0f : 0.0f;
    }

    public void ToggleCredits ()
	{
		if (creditsActive == false) 
		{
			creditsCanvas.SetActive (true);
			creditsActive = true;
		} 
		else 
		{
			creditsCanvas.SetActive (false);
			creditsActive = false;
		}
    }

    // Plays the game
    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    // Goes to a specified scene
    public void GoToScene(int sceneNum)
    {
        SceneManager.LoadScene(sceneNum);
    }

    // Exits the game
    public void ExitGame()
    {
        Application.Quit();
    }

    // Reloads the current scene
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    // Returns the game to the Main Menu scenes
    public void QuitToMenu()
    {
        SceneManager.LoadScene(0);
    }

    // Turns a menu object on and off
    public void UIObjectToggle(GameObject item)
    {
        item.SetActive(!item.activeSelf);
    }

    // ================================================================================= Button Functions =================================================================================


    // Get Instant Tokens
    public void GainTokens()
    {

    }

    public void GainWater()
    {

    }

    public void GainShovel()
    {

    }

    public void SkipPhase()
    {

    }
}
