﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class ShopScript : MonoBehaviour
{
    // Refs
    InventorySystem invSystem;
    NewPlayerScript player;
    ItemDatabase itemDB;
    public GameObject shopCanvas;
    Camera cam;

    // List of costs for an item and the items themselves
    /*
    public List<GameObject> itemCost1 = new List<GameObject>();
    public List<GameObject> itemCost2 = new List<GameObject>();
    public List<GameObject> itemCost3 = new List<GameObject>();
    */
    public int itemCost1;
    public int itemCost2;
    public int itemCost3;
    public int itemCost4;

    public GameObject item1;
    public GameObject item2;
    public GameObject item3;
    public GameObject item4;

    public GameObject goatSpawn;

    public Text shopText;

    // Use this for initialization
    void Start ()
    {
        invSystem = GameObject.FindWithTag("Player").GetComponent<InventorySystem>();
        player = GameObject.FindWithTag("Player").GetComponent<NewPlayerScript>();
        itemDB = GameObject.FindWithTag("GameController").GetComponent<ItemDatabase>();
        cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update ()
    {
        ShopUI();
    }

    void ShopUI()
    {
        // Billboard effect
        shopCanvas.transform.LookAt(shopCanvas.transform.position + cam.transform.rotation * Vector3.forward,
            cam.transform.rotation * Vector3.up);

        // If the player is nearby, Turn the UI on
        shopCanvas.SetActive((this.gameObject == player.nearbyShop) ? true : false);
    }

    // Adds the item to the player's inventory
    void AddItemToInventory(GameObject itemToAdd)
    {
        // If the player's inventory is not full...
        if (!invSystem.InventoryFull())
        {
            Item itemScript = itemToAdd.GetComponent<Item>();

            // For each slot in the inventory...
            for (int i = 0; i < invSystem.slots.Length; i++)
            {
                // If this slot (slot[i]) is not filled yet...
                if (!invSystem.slots[i].isFull)
                {
                    // Set it to true / it is now filled
                    invSystem.slots[i].isFull = true;

                    // Adds a button corresponding to the item added in the inventory
                    Instantiate(itemScript.itemButton, invSystem.slots[i].transform, false);

                    // Add the gameobject to the inventory
                    invSystem.playerInventory[i] = itemToAdd;

                    // Stop the loop
                    break;
                }
            }
        }
        else Debug.Log("No space!");
    }
    
    // Increments cost based on the int variable
    void IncreaseCost(int choice)
    {
        switch (choice)
        {
            case 1:
                itemCost1 = itemCost1 * 2;
                break;

            case 2:
                itemCost2 = itemCost2 * 2;
                break;

            case 3:
                itemCost3 = itemCost3 * 2;
                break;

            case 4:
                itemCost4 = itemCost4 * 2;
                break;

                //default:
                //break;
        }
    }

    // Returns true if the player has sufficient tokens in their inventory
    public bool PriceCompare(int itemPrice)
    {
        if (invSystem.tokens >= itemPrice)
            return true;
        else return false;
    }

    // ========================== ITEM BUTTON/EVENT TRIGGER FUNCTIONS ==========================

    public void ShowCost(int choice)
    {
        switch (choice)
        {
            case 1:
                shopText.text = "Banana Seed  Cost: " + itemCost1;
                break;
            case 2:
                shopText.text = "Mango Seed  Cost: " + itemCost2;
                break;
            case 3:
                shopText.text = "Shovel Repair  Cost: " + itemCost3;
                break;
            case 4:
                shopText.text = "Goat  Cost: " + itemCost4;
                break;
            default:
                shopText.text = "Cost: ";
                break;
        }
    }

    // UI Button function for item purchase
    public void PurchaseItem(int choice)
    {
        // Determines which item is being bought
        //List<GameObject> itemCost = null;
        int itemCost = 0;
        GameObject purchase = null;
        switch (choice)
        {
            case 1:
                itemCost = itemCost1;
                purchase = item1;
                break;

            case 2:
                itemCost = itemCost2;
                purchase = item2;
                break;

            case 3:
                itemCost = itemCost3;
                purchase = item3;
                break;

           case 4:
                itemCost = itemCost4;
                purchase = item4;
                break;

                //default:
                //break;
        }

        // if the player has the right amount of tokens
        if (PriceCompare(itemCost))
        {
            // If it's an inventory item...
            if (choice < 4)
                AddItemToInventory(purchase);   // Add the item to the player's inventory
            else Instantiate(purchase);   // Spawn a goat

            // Deduct the required amount and increment the price
            invSystem.tokens -= itemCost;
            IncreaseCost(choice);
        }
        else Debug.Log("You do not have the right things!");

        /*
        // If the player has the items in their inventory...
        if (DetectItem(itemCost))
        {
            // Remove the items from the inventory and deletes the associated inventory button
            for (int i = 0; i < itemCost.Count; i++)
            {
                for (int j = 0; j < invSystem.playerInventory.Count; j++)
                {
                    if (itemCost[i] == invSystem.playerInventory[j])
                    {
                        invSystem.playerInventory[j] = null;
                        Destroy(invSystem.slots[j].GetComponentInChildren<ItemButton>().gameObject);
                        invSystem.slots[j].isFull = false;
                        break;
                    }
                }
            }

            // Add the purchased item to the player's inventory
            AddItemToInventory(purchase);
        }
        else Debug.Log("You do not have the right things!");
        */
    }

    // Detects items from player's inventory
    public bool DetectItem(List<GameObject> itemCost)
    {
        // Creates a duplicate of the inventory
        List<GameObject> playerInvCopy = new List<GameObject>(invSystem.playerInventory);

        // For every item in itemCost
        for (int i = 0; i < itemCost.Count; i++)
        {
            // If there's a copy of it in the player's inventory, remove it and continue the loop
            if (playerInvCopy.Contains(itemCost[i]))
                playerInvCopy.Remove(itemCost[i]);
            else return false;   // If there is no copy, return false and end the function prematurely
        }
        return true;   // Returns true if all items are there
    }

    // ========================== Mouse Hover FUNCTIONS ==========================

    private void OnMouseEnter()
    {

    }
    private void OnMouseExit()
    {

    }
}
