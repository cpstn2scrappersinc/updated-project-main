﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    public List<GameObject> tutorialImages = new List<GameObject>();

    // Booleans

    // References
    GameObject player;
    NewPlayerScript playerScript;

    // Use this for initialization
    void Start()
    {
        // Set script refs
        player = GameObject.FindWithTag("Player");
        playerScript = player.GetComponent<NewPlayerScript>();

        // Set coroutines
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Triggers an image
    public void TriggerImage(float t, CanvasGroup toShow)
    {
        StartCoroutine(FadeInOutImage(t, toShow));
    }

    // Fades images in and out
    IEnumerator FadeInOutImage(float time, CanvasGroup toShow)
    {
        float i = 0;
        float rate = 1 / time;

        while (i < 1)
        {
            toShow.alpha = i;
            i += Time.deltaTime * rate;
            yield return 0;
        }
        yield return new WaitForSeconds(time);
        toShow.gameObject.SetActive(false);
        yield break;
    }
}
