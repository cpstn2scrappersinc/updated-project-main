﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialArrowScript : MonoBehaviour
{
    // Start is called before the first frame update


    public GameObject tutMsg;

    public float yPos;

	public float yInc;

	public float movementTimer;

	public Vector3 msgPos;

	void Start()
	{
		
	}

    void Update()
    {
		msgPos = new Vector3(transform.position.x - 0.5f, transform.position.y + 6, transform.position.z);
		Vector3 tutPos = Camera.main.WorldToScreenPoint(msgPos);
    	tutMsg.transform.position = tutPos;
		FloatingMovement();
    }

    public void FloatingMovement()
    {	
		this.transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
		movementTimer += Time.deltaTime;

		if (movementTimer <= 1)
			yPos += yInc * Time.deltaTime;
		else
			yPos -= yInc * Time.deltaTime;
		

		if (movementTimer >= 2)
			movementTimer = 0;
    }

    public void AdjustYPos(float newYPos)
    {
    	yPos = newYPos;
		this.transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
    }


}
