﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MvmentTutScript : MonoBehaviour
{
	//public GameObject player;
    // Start is called before the first frame update
    public Collider col;

    public GameObject tutMgr;

    void Start()
    {
        //player = GameObject.
        tutMgr = GameObject.Find("Tutorial Manager");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveToPos(float nPosX, float nPosZ)
    {
    	transform.position = new Vector3 (nPosX, 0 , nPosZ);
    }

    void OnTriggerEnter(Collider other)
    {
    	if(other.GetComponent<NewPlayerScript>())
    	{
			if(tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 1)
			{
				tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(1);
				col.enabled = !col.enabled;
    		}
			else if (tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 4)
			{
				col.enabled = !col.enabled;
				tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(4);
			}

			else if (tutMgr.GetComponent<NewTutorialMgrScript>().curTutIndex == 10)
			{
				col.enabled = !col.enabled;
				tutMgr.GetComponent<NewTutorialMgrScript>().DisplayTutorial(10);
			}
    	}
    }
}
