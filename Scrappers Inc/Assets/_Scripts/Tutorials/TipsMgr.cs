﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipsMgr : MonoBehaviour {

	public GameObject goatTipCanvas;
	public GameObject riceTipCanvas;
	public GameObject shovelTipCanvas;
   

	public bool goatTipShown;
	public bool riceTipShown;
	public bool shovelTipShown;

	public IEnumerator ShowGoatTip ()
	{
		if(goatTipShown == false)
		{
			Debug.Log ("Showing goat tip");
			goatTipShown = true;
			goatTipCanvas.SetActive(true);

			yield return new WaitForSeconds (7.50f);
			Debug.Log ("Hiding goat tip");
			goatTipCanvas.SetActive(false);
		}
	}

	public IEnumerator ShowShovelTip ()
	{
		if(shovelTipShown == false)
		{
			Debug.Log ("Showing shovel tip");
			shovelTipShown = true;
			shovelTipCanvas.SetActive(true);

			yield return new WaitForSeconds (7.5f);
			Debug.Log ("Hiding shovel tip");
			shovelTipCanvas.SetActive(false);
		}
	}

	public IEnumerator ShowRiceTip ()
	{
		if(shovelTipShown == false)
		{
			Debug.Log ("Showing rice tip");
			riceTipShown = true;
			riceTipCanvas.SetActive(true);

			yield return new WaitForSeconds (6.0f);
			Debug.Log ("Hiding rice tip");
			riceTipCanvas.SetActive(false);
		}
	}

}
