﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewTutorialMgrScript : MonoBehaviour 
{
	public GameObject tutImages;

	//public List<GameObject> tutorialPics;

	GameControllerScript gamCon;

	public int curPicIndex;
	public GameObject tutorialArrowA;
	//public GameObject tutorialArrowAMsg;

	public GameObject tutorialArrowB;


	public GameObject[] tutorialMsgsA; //3D tut msgs
	public GameObject[] tutorialMsgsB;

	public GameObject gameCtrlr;
    public GameObject wasdCanvas;
    public float picTimer;

    public Vector3 arrowAPos; //3D Arrow's position
    public Vector3 arrowBPos; //2D Arrow's position

    public GameObject movementTutSphere; //the sphere used in the movement tutorial for collision
    public GameObject well;
    public GameObject firstDigSpot;
	public GameObject secondDigSpot;

    public GameObject plantCoconutIcon;
	public GameObject submitBtn;
	public GameObject foodSupply;
	public GameObject coconutIcon;
	public GameObject fertilizeBtn;

	public GameObject scoreMgr;

	public GameObject tutorialFinishedNotif;

	Vector3 tutMsgPos;

    //public List<Image> tutImages; 

    public int curTutIndex;

    public bool introScene;

	public GameObject plantBananaTreeBtn;
    public GameObject plantMangoTreeBtn;
	

    // Use this for initialization
    void Start () 
	{
		gamCon = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();
		movementTutSphere = GameObject.Find("MovementTutorialCollider");
		scoreMgr = GameObject.Find("ScoreManager");
		DisplayTutorial(0);
	}
	
	// Update is called once per frame
	void Update () 
	{	
		//if(!gamCon.isTutorialDone)
			

		switch(curTutIndex)
		{
			case 1:
				tutorialArrowA.GetComponent<TutorialArrowScript>().tutMsg = tutorialMsgsA[0];
				tutMsgPos = new Vector3(movementTutSphere.transform.position.x, movementTutSphere.transform.position.y - 4,movementTutSphere.transform.position.z);
				tutorialMsgsA[0].transform.position = Camera.main.WorldToScreenPoint(tutMsgPos);
				break;
			case 2:
				
				tutMsgPos = new Vector3(movementTutSphere.transform.position.x, movementTutSphere.transform.position.y - 4,movementTutSphere.transform.position.z);
				tutorialMsgsA[1].transform.position = Camera.main.WorldToScreenPoint(tutMsgPos);
				break;
			case 3:
				tutMsgPos = new Vector3(movementTutSphere.transform.position.x, movementTutSphere.transform.position.y - 4,movementTutSphere.transform.position.z);
				tutorialMsgsA[2].transform.position = Camera.main.WorldToScreenPoint(tutMsgPos);
				break;
			case 4:
				tutorialArrowA.GetComponent<TutorialArrowScript>().tutMsg = tutorialMsgsA[2];
				break;
			case 5:
				tutorialArrowA.GetComponent<TutorialArrowScript>().tutMsg = tutorialMsgsA[3];
				break;
			case 6:
				tutorialArrowA.GetComponent<TutorialArrowScript>().tutMsg = tutorialMsgsA[4];
				break;
			case 7:
				tutMsgPos = new Vector3(firstDigSpot.transform.position.x, firstDigSpot.transform.position.y - 5, firstDigSpot.transform.position.z);
				tutorialMsgsA[5].transform.position = Camera.main.WorldToScreenPoint(tutMsgPos);
				break;
			case 8:
				tutMsgPos = new Vector3(firstDigSpot.transform.position.x, firstDigSpot.transform.position.y - 5, firstDigSpot.transform.position.z);
				tutorialMsgsA[6].transform.position = Camera.main.WorldToScreenPoint(tutMsgPos);
				break;
			case 9:
				tutorialArrowA.GetComponent<TutorialArrowScript>().tutMsg = tutorialMsgsA[7];
				break;
			case 10:
				tutorialArrowA.GetComponent<TutorialArrowScript>().tutMsg = tutorialMsgsA[8];
				break;
			case 11:
				tutorialArrowA.GetComponent<TutorialArrowScript>().tutMsg = tutorialMsgsA[9];

				break;
			case 12:
				tutMsgPos = new Vector3(secondDigSpot.transform.position.x, secondDigSpot.transform.position.y - 5, secondDigSpot.transform.position.z);
				tutorialMsgsA[10].transform.position = Camera.main.WorldToScreenPoint(tutMsgPos);
			break;
			
		}
	}

	public void DisplayTutorial(int tutIndex)
	{
		switch (tutIndex)
		{	//comments are where to call the function with the corresponding case
			case 0: //Start()
				Debug.Log("Displaying movement tutorial");
				tutorialArrowA.SetActive(true);
				tutorialMsgsA[0].SetActive(true);
				//tutorialArrowAMsg.SetActive(true);
				tutorialArrowA.transform.position = movementTutSphere.transform.position;
				tutorialArrowA.GetComponent<TutorialArrowScript>().AdjustYPos(3);
				break;
			case 1: //tutorial sphere ontriggerenter
				Debug.Log("Displaying digging tutorial");
				tutorialArrowA.SetActive(false);
				tutorialMsgsA[0].SetActive(false);
				tutorialMsgsA[1].SetActive(true);
				break;
			case 2: //DiggingScript - Dig()
				Debug.Log("Displaying planting tutorial");
				tutorialArrowA.SetActive(false);
				tutorialMsgsA[1].SetActive(false);
				tutorialArrowB.SetActive(true);
				tutorialMsgsB[0].SetActive(true);
				tutorialArrowB.transform.position = new Vector2 (plantCoconutIcon.transform.position.x - 75, plantCoconutIcon.transform.position.y);
				tutorialArrowB.transform.eulerAngles = new Vector3(tutorialArrowB.transform.rotation.x, tutorialArrowB.transform.rotation.y, -90);
				tutorialMsgsB[0].transform.position = new Vector2 (tutorialArrowB.transform.position.x - 175, tutorialArrowB.transform.position.y);
				break;
			case 3:////
				Debug.Log("Displaying walk to well tutorial");
				movementTutSphere.GetComponent<MvmentTutScript>().col.enabled = !movementTutSphere.GetComponent<MvmentTutScript>().col.enabled;
				movementTutSphere.GetComponent<MvmentTutScript>().MoveToPos(3, -11);
				tutorialArrowB.SetActive(false);
				tutorialArrowA.SetActive(true);
				tutorialArrowA.transform.position = movementTutSphere.transform.position;
				tutorialArrowA.GetComponent<TutorialArrowScript>().AdjustYPos(3);
				tutorialMsgsB[0].SetActive(false);
				tutorialMsgsA[2].SetActive(true);

				break;

			case 4: //SkillScript - PlantSeed()
				Debug.Log("Displaying water gathering tutorial");
				tutorialArrowA.SetActive(true);

				tutorialMsgsA[2].SetActive(false);
				tutorialMsgsA[3].SetActive(true);
				tutorialArrowA.transform.position = well.transform.position;
				tutorialArrowA.GetComponent<TutorialArrowScript>().AdjustYPos(2);
			
				tutorialArrowB.SetActive(false);
				tutorialMsgsB[0].SetActive(false);
				break;

			case 5: // watering script - water gathering
				Debug.Log("Displaying approach dig spot tutorial");
				tutorialMsgsA[3].SetActive(false);
				tutorialMsgsA[4].SetActive(true);
				tutorialArrowA.transform.position = firstDigSpot.transform.position;
				break;

			case 6: //main tree script - ontriggerenter
				Debug.Log("Displaying watering tutorial");
				tutorialMsgsA[4].SetActive(false);
				tutorialMsgsA[5].SetActive(true);
				tutorialArrowA.transform.position = firstDigSpot.transform.position;
				tutorialArrowA.GetComponent<TutorialArrowScript>().AdjustYPos(4);

				break;
			case 7: //MainTreeScript - Growth()
				Debug.Log("Displaying keep watering tutorial");
				tutorialArrowA.SetActive(false);
				tutorialMsgsA[5].SetActive(false);
				tutorialMsgsA[6].SetActive(true);

				break;
			case 8: //bear fruit
				Debug.Log("Displaying fruit gathering tutorial");
				tutorialArrowA.SetActive(true);
				tutorialArrowA.GetComponent<TutorialArrowScript>().AdjustYPos(4);
				tutorialMsgsA[6].SetActive(false);
				tutorialMsgsA[7].SetActive(true);
				//tutorialArrowAMsg.transform;
				//tutorialArrowAMsg.SetActive(true);
				break;

			case 9: // after gathering fruit
				Debug.Log("Displaying walk here 2 tutorial");
				movementTutSphere.GetComponent<MvmentTutScript>().col.enabled = !movementTutSphere.GetComponent<MvmentTutScript>().col.enabled;
				movementTutSphere.GetComponent<MvmentTutScript>().MoveToPos(1, -6);
				tutorialArrowA.transform.position = movementTutSphere.transform.position;
				tutorialMsgsA[7].SetActive(false);
				tutorialMsgsA[8].SetActive(true);
				break;

			case 10: //movement tut script - ontriggerenter
				Debug.Log("Displaying dig again tutorial");
				tutorialArrowA.transform.position = movementTutSphere.transform.position;
				tutorialMsgsA[8].SetActive(false);
				tutorialMsgsA[9].SetActive(true);
				break;

			case 11: ////
				Debug.Log("Displaying plant second tree tutorial");
				tutorialMsgsA[9].SetActive(false);
				tutorialArrowA.SetActive(false);

				tutorialArrowB.SetActive(true);
				tutorialMsgsB[1].SetActive(true);
				tutorialArrowB.transform.position = new Vector2 (plantCoconutIcon.transform.position.x - 75, plantCoconutIcon.transform.position.y);
				tutorialArrowB.transform.eulerAngles = new Vector3(tutorialArrowB.transform.rotation.x, tutorialArrowB.transform.rotation.y, -90);
				tutorialMsgsB[1].transform.position = new Vector2 (tutorialArrowB.transform.position.x - 175, tutorialArrowB.transform.position.y);
				break;

			case 12: ////
				Debug.Log("Displaying keep watering again tutorial");
				tutorialArrowB.SetActive(false);
				tutorialMsgsB[1].SetActive(false);

				tutorialArrowA.SetActive(false);

				//tutorialMsgsA[10].SetActive(true);

				break;

			case 13: //TreeFruitScript - AddFruit()
				Debug.Log("Displaying fruit eating tutorial");
				tutorialArrowB.SetActive(true);
				tutorialArrowB.transform.position = new Vector2 (coconutIcon.transform.position.x, coconutIcon.transform.position.y + 100);
				tutorialArrowB.transform.eulerAngles = new Vector3(tutorialArrowB.transform.rotation.x, tutorialArrowB.transform.rotation.y, 180);

				tutorialMsgsB[2].SetActive(false);
				tutorialMsgsB[3].SetActive(true);
				tutorialMsgsB[3].transform.position = new Vector2 (tutorialArrowB.transform.position.x, tutorialArrowB.transform.position.y + 125);
				break;
			case 14: //ItemButton - UseItem()
				Debug.Log("Displaying fertilize tutorial");

				tutorialArrowA.SetActive(true);
				tutorialArrowB.SetActive(false);

				tutorialArrowA.transform.position = firstDigSpot.transform.position;
				tutorialArrowA.GetComponent<TutorialArrowScript>().AdjustYPos(4);

				tutorialArrowB.SetActive(true);
				tutorialArrowB.transform.position = new Vector2 (fertilizeBtn.transform.position.x + 75, fertilizeBtn.transform.position.y);
				tutorialArrowB.transform.eulerAngles = new Vector3(tutorialArrowB.transform.rotation.x, tutorialArrowB.transform.rotation.y, 90);
				tutorialMsgsB[4].transform.position = new Vector2 (tutorialArrowB.transform.position.x + 175, tutorialArrowB.transform.position.y);

				tutorialMsgsB[3].SetActive(false);
				tutorialMsgsB[4].SetActive(true);
				tutorialMsgsB[4].transform.position = new Vector2 (tutorialArrowB.transform.position.x + 75, tutorialArrowB.transform.position.y + 125);
				break;

			case 15: //TreeFruitScript - AddFruit()
				Debug.Log("Displaying order submission tutorial");
				EndTutorial();
				for(int i = 0; i < tutorialMsgsA.Length; i++)
					tutorialMsgsA[i].SetActive(false);

				for(int i = 0; i < tutorialMsgsB.Length; i++)
					tutorialMsgsB[i].SetActive(false);

				tutorialArrowA.SetActive(false);
				tutorialMsgsA[10].SetActive(false);
				tutorialArrowB.SetActive(true);
				tutorialArrowB.transform.position = new Vector2 (submitBtn.transform.position.x - 125, submitBtn.transform.position.y - 100);
				tutorialArrowB.transform.eulerAngles = new Vector3(tutorialArrowB.transform.rotation.x, tutorialArrowB.transform.rotation.y, - 90);
				tutorialMsgsB[2].SetActive(true);
				tutorialMsgsB[2].transform.position = new Vector2 (tutorialArrowB.transform.position.x - 200, tutorialArrowB.transform.position.y);
				break;

			case 16:
				
				tutorialArrowA.SetActive(false);
				tutorialArrowB.SetActive(false);

				for(int i = 0; i < tutorialMsgsA.Length; i++)
					tutorialMsgsA[i].SetActive(false);

				for(int i = 0; i < tutorialMsgsB.Length; i++)
					tutorialMsgsB[i].SetActive(false);

				break;

		
	}
		curTutIndex += 1;
    }

    public void FinishTutorial()
    {
		Debug.Log("Tutorial ended");
		plantMangoTreeBtn.SetActive(true);
		plantBananaTreeBtn.SetActive(true);
		scoreMgr.GetComponent<ScoreMgrScript>().IncreaseScore(250);
		StartCoroutine(DisplayTutorialEndedNotif());
		tutorialArrowA.SetActive(false);
		tutorialArrowB.SetActive(false);

		for(int i = 0; i < tutorialMsgsA.Length; i++)
			tutorialMsgsA[i].SetActive(false);

		for(int i = 0; i < tutorialMsgsB.Length; i++)
			tutorialMsgsB[i].SetActive(false);
    }

    public void EndTutorial()
    {
		gamCon.ShowBar();
        gamCon.isTutorialDone = true;
    }

    public IEnumerator DisplayTutorialEndedNotif()
    {
    	tutorialFinishedNotif.SetActive(true);
    	yield return new WaitForSeconds(8);
		tutorialFinishedNotif.SetActive(false);
    }
}